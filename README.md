Sidecar is a series of high level celery tasks, [ewoks](https://gitlab.esrf.fr/workflow/ewoks) nodes, plugins, and helpers to run auto processing in the context of [daiquiri](https://gitlab.esrf.fr/ui/daiquiri) and [mimosa](https://gitlab.esrf.fr/ui/mimosa/mimosa)

Documentation can be found [here](https://workflow.gitlab-pages.esrf.fr/sidecar)
