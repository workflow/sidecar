import logging

from ewokscore import Task
from ...mimosa import metadata as get_metadata

logger = logging.getLogger(__name__)


class GetMetadata(
    Task,
    input_names=["dataCollectionId"],
    output_names=["metadata"],
):
    def run(self) -> None:
        metadata = get_metadata(self.inputs.dataCollectionId)
        self.outputs.metadata = metadata
