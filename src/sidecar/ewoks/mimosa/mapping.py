import logging

from ewokscore import Task

from ...mimosa.mapping import add_map, add_mapping_roi
from ...mimosa import metadata as get_metadata

logger = logging.getLogger(__name__)


class AddMap(
    Task,
    input_names=[
        "dataCollectionId",
        "xrfFluorescenceMappingROIId",
        "data",
    ],
    optional_input_names=["points", "dataFormat", "autoProcProgramId"],
    output_names=["xrfFluorescenceMappingId"],
):
    """Create a `XRFFluorescenceMapping`"""

    def run(self) -> None:
        metadata = get_metadata(self.inputs.dataCollectionId)

        self.outputs.xrfFluorescenceMappingId = add_map(
            xrfFluorescenceMappingROIId=self.inputs.xrfFluorescenceMappingROIId,
            gridInfoId=metadata["gridInfoId"],
            data=self.inputs.data if self.inputs.data else None,
            points=self.inputs.points if self.inputs.points else None,
            autoProcProgramId=self.inputs.autoProcProgramId
            if self.inputs.autoProcProgramId
            else None,
        )


class AddMappingROI(
    Task,
    input_names=[
        "startEnergy",
        "endEnergy",
    ],
    optional_input_names=["element", "edge", "scalar", "blSampleId"],
    output_names=["xrfFluorescenceMappingROIId"],
):
    """Create a `XRFFluorescenceMappingROI`"""

    def run(self) -> None:
        self.outputs.xrfFluorescenceMappingROIId = add_mapping_roi(
            startEnergy=self.inputs.startEnergy,
            endEnergy=self.inputs.endEnergy,
            element=self.inputs.element if self.inputs.element else None,
            edge=self.inputs.edge if self.inputs.edge else None,
            scalar=self.inputs.scalar if self.inputs.scalar else None,
            blSampleId=self.inputs.blSampleId if self.inputs.blSampleId else None,
        )
