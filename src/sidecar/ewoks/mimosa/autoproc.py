import os
import logging

from ewokscore import Task

from ...mimosa.autoproc import (
    create_processing_job,
    update_autoproc_program,
    add_autoproc_attachment,
    add_autoproc_message,
)
from ...mimosa import metadata as get_metadata

logger = logging.getLogger(__name__)


class StartJob(
    Task,
    input_names=["output_filename", "process_name"],
    optional_input_names=["dataCollectionId", "workflow", "config", "automatic"],
    output_names=["autoProcProgramId", "processingJobId", "output_directory"],
):
    """Create a `ProcessingJob` and `AutoProcProgram`"""

    def run(self) -> None:
        metadata = get_metadata(self.inputs.dataCollectionId)
        if not metadata:
            raise RuntimeError(
                f"No such dataCollectionId: {self.inputs.dataCollectionId}"
            )

        processingJobId, autoProcProgramId = create_processing_job(
            {
                "dataCollectionId": self.inputs.dataCollectionId,
                "recipe": self.inputs.process_name,
                "automatic": (
                    True if self.missing_inputs.automatic else self.inputs.automatic
                ),
                "parameters": {
                    "workflow": self.inputs.workflow,
                },
            }
        )

        self.outputs.processingJobId = processingJobId
        self.outputs.autoProcProgramId = autoProcProgramId
        self.outputs.output_directory = os.path.dirname(self.inputs.output_filename)

        update_autoproc_program(
            autoProcProgramId,
            {
                "processingStartTime": True,
                "processingMessage": "processing started",
            },
        )

        logger.info(
            f"*Started Job* processingJobId: {processingJobId} autoProcProgramId: {autoProcProgramId}"
        )


class EndJob(
    Task,
    optional_input_names=["nxdata_url", "autoProcProgramId"],
):
    """Finalise an `AutoProcProgram` and mark as succeeded"""

    def run(self) -> None:
        nxdata_url = self.inputs.nxdata_url
        if not nxdata_url:
            return

        if not self.inputs.autoProcProgramId:
            print("sidecar: No autoProcProgramId, not processing")
            return

        update_autoproc_program(
            self.inputs.autoProcProgramId,
            {
                "processingEndTime": True,
                "processingStatus": 1,
                "processingMessage": "processing success",
            },
        )
        logger.info(
            f"*Job Finished* autoProcProgramId: {self.inputs.autoProcProgramId}"
        )


class ErrorHandler(
    Task,
    optional_input_names=["WorkflowException", "autoProcProgramId", "output_directory"],
):
    """Catch workflow failure and mark `AutoProcProgram` as such
    Writes the exception and traceback to log file
    """

    def run(self) -> None:
        exception = self.inputs.WorkflowException
        if not exception:
            return

        if not self.inputs.autoProcProgramId:
            print("sidecar: No autoProcProgramId, not processing")
            return

        autoProcProgramId = self.inputs.autoProcProgramId
        update_autoproc_program(
            autoProcProgramId,
            {
                "processingEndTime": True,
                "processingStatus": 0,
                "processingMessage": "processing failed",
            },
        )

        exception_log = f"{self.inputs.output_directory}/exception.log"
        with open(exception_log, "w") as log_file:
            log_file.write("\n".join(exception["traceBack"]))

        add_autoproc_attachment(
            autoProcProgramId,
            os.path.dirname(exception_log),
            os.path.basename(exception_log),
            "log",
        )

        # raise RuntimeError(exception)


class AddProcessingJob(
    Task,
    input_names=["dataCollectionId", "graph", "job_id"],
    optional_input_names=["automatic", "parameters"],
    output_names=["processingJobId", "autoProcProgramId"],
):
    """Create a new `ProcessingJob`"""

    def run(self) -> None:
        job_id = self.request.id

        parameters = self.inputs.parameters if self.inputs.parameters else {}

        processingJobId, autoProcProgramId = create_processing_job(
            {
                "dataCollectionId": self.inputs.dataCollectionId,
                "recipe": os.path.basename(self.inputs.graph).replace(".json", ""),
                "automatic": (
                    self.inputs.automatic if self.inputs.automatic is not None else True
                ),
                "parameters": {"job_id": job_id, **parameters},
            }
        )

        self.outputs.processingJobId = processingJobId
        self.outputs.autoProcProgramId = autoProcProgramId


class AddAttachment(
    Task,
    optional_input_names=["autoProcProgramId", "nxdata_url", "importanceRank"],
    output_names=["autoProcProgramAttachmentId"],
):
    """Add an attachment to an `AutoProcProgramId` from an nxdata_url"""

    def run(self) -> None:
        nxdata_url = self.inputs.nxdata_url
        if not nxdata_url:
            return

        if not self.inputs.autoProcProgramId:
            print("sidecar: No autoProcProgramId, not processing")
            return

        filename, _ = nxdata_url.split("::")
        self.outputs.autoProcProgramAttachmentId = add_autoproc_attachment(
            self.inputs.autoProcProgramId,
            os.path.dirname(filename),
            os.path.basename(filename),
            "result",
            self.inputs.importanceRank if self.inputs.importanceRank else None,
        )


class AddMessage(
    Task,
    optional_input_names=["autoProcProgramId", "severity", "message", "description"],
    output_names=["autoProcProgramMessageId"],
):
    """Add a message to an `AutoProcProgramId`"""

    def run(self) -> None:
        if not self.inputs.autoProcProgramId:
            print("sidecar: No autoProcProgramId, not processing")
            return

        self.outputs.autoProcProgramMessageId = add_autoproc_message(
            self.inputs.autoProcProgramId,
            self.inputs.severity,
            self.inputs.message,
            self.inputs.description,
        )
