import logging
import math
from typing import List, Optional

import numpy
from ewokscore import Task
import silx.io.h5py_utils

from ...mimosa import models
from ...mimosa.mapping import get_mapping_rois, add_mapping_roi, get_maps, add_map

logger = logging.getLogger(__name__)


def get_scalar_dataset(file: str, path: str, scalar_name: str) -> List[float]:
    with silx.io.h5py_utils.File(file) as h5:
        root = h5[path]
        if scalar_name not in root:
            raise RuntimeError(
                f"Selected scalar `{scalar_name}` does not exist in `{file}::{path}`"
            )

        group = h5[f"{path}/{scalar_name}"]
        if len(group.shape) != 1:
            raise RuntimeError(
                f"Selected scalar `{scalar_name}` is not a scalar. Shape={group.shape}"
            )

        data: List[float] = group[()].tolist()
        return data


class CreateScalarMap(
    Task,
    input_names=["metadata", "scalar_name"],
    output_names=["map_ids"],
):
    def run(self) -> None:
        if not isinstance(self.inputs.metadata, dict):
            raise RuntimeError("`metadata` should be a dict")

        metadata = self.inputs.metadata
        file = f"{metadata['imageDirectory']}/{metadata['fileTemplate']}"
        path = metadata["imageContainerSubPath"]

        scalar_name = self.inputs.scalar_name
        maps = get_maps(self.inputs.dataCollectionId)
        for map_ in maps:
            if map_.XRFFluorescenceMappingROI.scalar == scalar_name:
                raise RuntimeError(
                    f"Scalar Map already exists with xrfFluorescenceMappingROIId {map_.xrfFluorescenceMappingId} for scalar `{scalar_name}` and data collection {self.inputs.dataCollectionId}"
                )

        data = get_scalar_dataset(file, path, scalar_name)
        points = metadata["steps_x"] * metadata["steps_y"]

        if len(data) < points:
            missing = points - len(data)
            data.extend([-1 for _ in range(missing)])

        mappingROIId = add_mapping_roi(startEnergy=0, endEnergy=0, scalar=scalar_name)
        map_id = add_map(
            xrfFluorescenceMappingROIId=mappingROIId,
            gridInfoId=metadata["gridInfoId"],
            points=points,
            data=data,
            autoProcProgramId=self.inputs.autoProcProgramId,
        )

        self.outputs.map_ids = [map_id]


def check_map_exists(
    mappingROIId: int, maps: List[models.XRFFluorescenceMapping]
) -> Optional[int]:
    if maps:
        for map_ in maps:
            if mappingROIId == map_.xrfFluorescenceMappingROIId:
                return map_.xrfFluorescenceMappingId

    return None


def get_detector_data(file: str, path: str) -> List[numpy.ndarray]:
    detectors = []
    with silx.io.h5py_utils.File(file) as h5:
        for gname in h5[path]:
            group = h5[f"{path}/{gname}"]
            if len(group.shape) == 2:
                detectors.append(group[()].tolist())

    return detectors


class CreateROIMapFromSpectra(
    Task,
    input_names=["metadata"],
    optional_input_names=["offset", "ev_per_bin"],
    output_names=["map_ids"],
):
    def run(self) -> None:
        if not isinstance(self.inputs.metadata, dict):
            raise RuntimeError("`metadata` should be a dict")

        metadata = self.inputs.metadata
        file = f"{metadata['imageDirectory']}/{metadata['fileTemplate']}"
        path = metadata["imageContainerSubPath"]

        offset = self.inputs.offset if self.inputs.offset else 0
        ev_per_bin = self.inputs.ev_per_bin if self.inputs.ev_per_bin else 5

        detectors = get_detector_data(file, path)
        detector = detectors[0]

        rois = get_mapping_rois(metadata["blSampleId"])
        if not rois:
            logger.warning(
                f"No mapping ROIs defined for sample `{metadata['blSample']}` [{metadata['blSampleId']}]"
            )

        maps = get_maps(self.inputs.dataCollectionId)

        points = metadata["steps_x"] * metadata["steps_y"]
        available_points = len(detector)

        map_ids = []
        for r in rois:
            existing_id = check_map_exists(r.xrfFluorescenceMappingROIId, maps)
            if existing_id:
                logger.info(
                    f"Skipping ROI `{r.xrfFluorescenceMappingROIId}` map already exists `{existing_id}`"
                )
                continue

            roi = []
            for p in range(points):
                if p < available_points:
                    start = max(0, math.floor((r.startEnergy - offset) / ev_per_bin))
                    end = min(
                        math.ceil((r.endEnergy - offset) / ev_per_bin), len(detector[p])
                    )
                    roi.append(int(numpy.sum(detector[p][start:end])))

                else:
                    roi.append(-1)

            logger.info("Creating map for ROI `{r.element}-{r.edge}`")
            map_id = add_map(
                xrfFluorescenceMappingROIId=r.xrfFluorescenceMappingROIId,
                gridInfoId=metadata["gridInfoId"],
                points=points,
                data=roi,
                autoProcProgramId=self.inputs.autoProcProgramId,
            )
            map_ids.append(map_id)

        if not map_ids:
            logger.warning("No new maps were created")

        self.outputs.map_ids = map_ids
