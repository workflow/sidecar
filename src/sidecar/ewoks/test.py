import logging
import random
import time

from ewokscore import Task

logger = logging.getLogger(__name__)


class TestProcess(
    Task,
    output_names=["finished", "array"],
    optional_input_names=[
        "raise_exception",
        "sleep_time",
        "random",
    ],
):
    def run(self) -> None:
        logger.info("TestProcess")
        logger.info(self.input_values)
        time.sleep(int(self.inputs.sleep_time) if self.inputs.sleep_time else 1)

        if self.inputs.raise_exception:
            raise RuntimeError("RunTimeError")

        self.outputs.finished = 1
        if self.inputs.random:
            output = [random.random() * 100 for i in range(100)]  # nosec
        else:
            output = [1, 2, 3, 4, 5]
        self.outputs.array = output
