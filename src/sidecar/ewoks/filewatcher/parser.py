from ewokscore import Task


class ParseMetadata(
    Task,
    input_names=["metadata"],
    output_names=[
        "file_path",
        "expected_points",
        "hdf5_path",
        "expected_per_point_delay",
    ],
):
    def run(self) -> None:
        if not isinstance(self.inputs.metadata, dict):
            raise RuntimeError("`metadata` should be a dict")

        metadata = self.inputs.metadata
        self.outputs.file_path = (
            f"{metadata['imageDirectory']}/{metadata['fileTemplate']}"
        )
        self.outputs.hdf5_path = metadata["imageContainerSubPath"]
        self.outputs.expected_points = metadata["numberOfImages"]
        self.outputs.expected_per_point_delay = metadata["exposureTime"]
