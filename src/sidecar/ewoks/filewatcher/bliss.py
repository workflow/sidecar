from ewokscore import Task
from silx.utils.retry import RetryTimeoutError
import logging
import os
import time

from .base import get_last_scan, get_hdf5_current_point
from .profiler import Profiler


logger = logging.getLogger(__name__)


class BlissFileWatcher(
    Task,
    input_names=["file_path", "hdf5_path", "expected_points"],
    optional_input_names=[
        "timeout",
        "expected_per_point_delay",
        "delay",
        "blocking",
        "status",
    ],
    output_names=["finished", "running"],
):
    def run(self) -> None:
        profiler = Profiler()

        # Whether this iteration is blocking or looping
        blocking = (  # noqa
            self.inputs.blocking if self.inputs.blocking is not None else True
        )

        hdf5_path = self.inputs.hdf5_path
        # Timeout defined by number of points (with 100% extra)
        if self.inputs.expected_per_point_delay:
            timeout = (
                self.inputs.expected_per_point_delay * self.inputs.expected_points * 2
            )
        # Or by explicit delay
        else:
            timeout = self.inputs.timeout if self.inputs.timeout else 200

        delay = self.inputs.delay if self.inputs.delay else 1

        last_file_exists = False
        points_seen = 0
        finished = False

        start_time = time.time()
        while not finished:
            now = time.time()
            if now - start_time > timeout:
                raise RuntimeError(
                    f"Timeout waiting for {self.inputs.expected_points} in {self.inputs.file_path}, found {points_seen}"
                )

            file_exists = True
            with profiler.record():
                if not os.path.isfile(self.inputs.file_path):
                    logger.info("File does not yet exist %s", self.inputs.file_path)
                    points_found = 0
                    file_exists = False

            # In case of sequence of scans, find latest scan,
            # only on the first message, and persist into status
            if not last_file_exists and file_exists:
                try:
                    with profiler.record():
                        hdf5_path = get_last_scan(
                            self.inputs.file_path, self.inputs.hdf5_path
                        )
                except KeyError as e:
                    logger.info(
                        "Unable to lookup potential sequence, cant find root: %s", e
                    )
                    points_found = 0
                    file_exists = False
                except Exception:
                    logger.exception("Something went wrong reading the root file")
                    points_found = 0
                    file_exists = False

            last_file_exists = file_exists

            if file_exists:
                try:
                    with profiler.record():
                        points_found = (
                            get_hdf5_current_point(self.inputs.file_path, hdf5_path)
                            - points_seen
                        )
                except RetryTimeoutError:
                    logger.exception("Could not read hdf5 file within timeout")
                    points_found = 0

            extra_log = {
                "delay": time.time() - start_time,
                "stat-time-max": profiler.max,
                "stat-time-mean": profiler.mean,
            }

            if self.inputs.expected_per_point_delay:
                expected_delay = (
                    self.inputs.expected_per_point_delay * self.inputs.expected_points
                )
                if extra_log["delay"] > expected_delay:
                    extra_log["unexpected-delay"] = extra_log["delay"] - expected_delay

            points_seen += points_found
            logger.info(
                f"Points found this iteration: {points_found}, total seen {points_seen}/{self.inputs.expected_points}"
            )

            if points_seen == self.inputs.expected_points:
                took = now - start_time
                finished = True
                self.outputs.finished = True
                logger.info(
                    f"Found {self.inputs.expected_points} in {took}s for {self.inputs.file_path}:{hdf5_path}",
                    extra=extra_log,
                )

            time.sleep(delay)
