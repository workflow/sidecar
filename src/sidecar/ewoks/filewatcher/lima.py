from ewokscore import Task
from silx.utils.retry import RetryTimeoutError
import logging
import time

from .profiler import Profiler
from .base import get_lima_current_point


logger = logging.getLogger(__name__)


class LimaFileWatcher(
    Task,
    input_names=["image_path", "expected_images"],
    optional_input_names=["timeout", "expected_per_image_delay", "delay", "blocking"],
    output_names=["finished", "running"],
):
    def run(self) -> None:
        profiler = Profiler()

        # Whether this iteration is blocking or looping
        blocking = (  # noqa
            self.inputs.blocking if self.inputs.blocking is not None else True
        )

        # Timeout defined by number of images (with 100% extra)
        if self.inputs.expected_per_image_delay:
            timeout = (
                self.inputs.expected_per_image_delay * self.inputs.expected_points * 2
            )
        # Or by explicit delay
        else:
            timeout = self.inputs.timeout if self.inputs.timeout else 200

        delay = self.inputs.delay if self.inputs.delay else 1

        images_seen = 0
        finished = False

        start_time = time.time()
        while not finished:
            now = time.time()
            if now - start_time > timeout:
                raise RuntimeError(
                    f"Timeout waiting for {self.inputs.expected_images} in {self.inputs.image_path}, found {images_seen}"
                )

            try:
                images_found, _ = get_lima_current_point(
                    self.inputs.image_path, profiler
                )
                images_found -= images_seen
            except RetryTimeoutError:
                self.log.exception("Could not read lima images within timeout")
                images_found = 0

            extra_log = {
                "delay": time.time() - start_time,
                "stat-time-max": profiler.max,
                "stat-time-mean": profiler.mean,
            }

            if self.inputs.expected_per_image_delay:
                expected_delay = (
                    self.inputs.expected_per_image_delay * self.inputs.expected_points
                )
                if extra_log["delay"] > expected_delay:
                    extra_log["unexpected-delay"] = extra_log["delay"] - expected_delay

            images_seen += images_found
            logger.info(
                f"Images found this iteration: {images_found}, total seen {images_seen}/{self.inputs.expected_images}"
            )

            if images_seen == self.inputs.expected_images:
                took = now - start_time
                finished = True
                self.outputs.finished = True
                logger.info(
                    f"Found {self.inputs.expected_images} in {took}s for {self.inputs.image_path}",
                    extra=extra_log,
                )

            time.sleep(delay)
