import glob
import logging
import math
import os
import re
import silx.io.h5py_utils
from ewokscore.events.global_state import EWOKS_EVENT_LOGGER_NAME

from .profiler import Profiler


logger = logging.getLogger(EWOKS_EVENT_LOGGER_NAME)


@silx.io.h5py_utils.retry(retry_timeout=2)  # type: ignore
def get_hdf5_current_point(file: str, path: str) -> int:
    """Iterate through hdf5

    Find smallest number of points in all datasets

    Args:
        file(str): Hdf5 file path
        path(str): Path to data within hdf5 file

    Returns
        min_points(int): Minimum number of points available
    """
    with silx.io.h5py_utils.File(file) as h5:
        group = h5[path]
        min_points = math.inf
        names = list(group.keys())
        for dataset in names:
            try:
                d = group[dataset]
            except KeyError:
                continue  # link destination does not exist yet

            point = d.shape[0]
            if point < min_points:
                min_points = point

        return int(min_points)


@silx.io.h5py_utils.retry(retry_timeout=2)  # type: ignore
def get_last_scan(file: str, path: str) -> str:
    """Get the latest scan from a hdf5 file if the first scan is a sequence"""
    with silx.io.h5py_utils.File(file) as h5:
        base = h5["1.1"]
        root = h5["1.1/measurement"]

        scan_numbers = None
        if "scan_numbers" in root:
            scan_numbers = root["scan_numbers"][()]

        if base["title"][()] == b"sequence_of_scans" and scan_numbers is not None:
            if len(scan_numbers) > 0:
                new_path = f"{scan_numbers[-1]}.1/measurement"
                logger.info(
                    f"File is sequence of scans, changing path to latest scan: {new_path}"
                )
                return new_path
            else:
                logger.info(f"Scan numbers has zero length for {file}")

        return path


@silx.io.h5py_utils.retry(retry_timeout=2)  # type: ignore
def get_lima_current_point(
    image_path: str, profiler: Profiler
) -> tuple[int, dict[int, str]]:
    """Get the latest point from a lima device"""
    total = 0
    image_containers = sorted(glob.glob(image_path + "*"))

    image_files = {}
    for container in image_containers:
        _, ext = os.path.splitext(container)

        # If the file is a h5 search for data within
        if ext == ".h5":
            with profiler.record():
                with silx.io.h5py_utils.File(container) as h5:
                    data = h5["entry_0000/measurement/data"]
                    found = data.shape[0]
                    for i in range(found):
                        image_files[total + i] = container
                    total += found

        # For individual images check they exist
        else:
            result = re.match(image_path + r"(\d+).", container)
            if result:
                with profiler.record():
                    if os.path.isfile(container):
                        image_files[int(result[1])] = container
                        total += 1

    return total, image_files
