import contextlib
import time
from typing import Iterator, Optional


class Profiler:
    """
    A helper class that can record summary statistics on time spent in
    code blocks. Example usage:

    profiler = _Profiler()
    with profiler.record():
        ...
    print(profiler.mean)
    print(profiler.max)
    """

    def __init__(self) -> None:
        self._timing_max = 0.0
        self._timing_sum = 0.0
        self._timing_count = 0.0

    @contextlib.contextmanager
    def record(self) -> Iterator[None]:
        start = time.time()
        try:
            yield
        finally:
            runtime = time.time() - start
            if self._timing_count:
                self._timing_count += 1
                self._timing_sum += runtime
                if runtime > self._timing_max:
                    self._timing_max = runtime
            else:
                self._timing_count = 1
                self._timing_sum = runtime
                self._timing_max = runtime

    @property
    def max(self) -> float:
        return self._timing_max

    @property
    def mean(self) -> Optional[float]:
        if not self._timing_count:
            return None
        return self._timing_sum / self._timing_count
