from dataclasses import dataclass
import os
import logging
from typing import List, Optional

import chemparse
from ewokscore import Task
import silx.io.h5py_utils

from ...settings import settings
from ...mimosa.autoproc import add_autoproc_message, add_autoproc_attachment
from ...mimosa.mapping import add_mapping_roi, add_map

logger = logging.getLogger(__name__)


@dataclass
class DataSets:
    detectors: List[str]
    livetimes: Optional[List[str]] = None
    monitor: Optional[str] = None

    @property
    def first_detector(self) -> str:
        return self.detectors[0]


def get_datasets(file: str, path: str, monitors: List[str] = ["iodet"]) -> DataSets:
    detectors = []
    livetimes = []
    monitor = None

    with silx.io.h5py_utils.File(file) as h5:
        for gname in h5[path]:
            group = h5[f"{path}/{gname}"]
            if len(group.shape) == 2:
                detectors.append(gname)

                livetime_group_name = f"{gname}_live_time"
                livetime_group_path = f"{path}/{livetime_group_name}"
                if livetime_group_path in h5:
                    livetimes.append(livetime_group_name)

            if gname in monitors:
                monitor = gname

    return DataSets(detectors=detectors, monitor=monitor, livetimes=livetimes)


@dataclass
class MapResult:
    peak: str
    data: List[float]


def parse_results(file: str, path: str, elements: List[str]) -> List[MapResult]:
    maps = []
    try:
        with silx.io.h5py_utils.File(file) as h5:
            root = h5[f"{path}/parameters"]
            for dataset in root:
                for element in elements:
                    if (
                        dataset.startswith(f"{element}_")
                        and not dataset.endswith("errors")
                        and not dataset.startswith("Scatter")
                    ):
                        # TODO: deal with multiple spectra (fluoxas)
                        maps.append(
                            MapResult(
                                peak=dataset.split("_"),
                                data=root[dataset][0].tolist(),
                            )
                        )
    except Exception:
        logger.exception("Could not parse results")
        raise

    return maps


class ParseMetadata(
    Task,
    input_names=["metadata", "taskDirectory", "autoProcProgramId"],
    output_names=[
        "scan_uri",
        "detector_name",
        "config",
        "output_uri_template",
        "quantification",
        "normalize_uris",
        "normalize_reference_values",
        "norm_output_uri",
    ],
):
    def run(self) -> None:
        if not isinstance(self.inputs.metadata, dict):
            raise RuntimeError("`metadata` should be a dict")

        metadata = self.inputs.metadata

        if not metadata["sequence"]:
            add_autoproc_message(
                self.inputs.autoProcProgramId,
                "WARNING",
                "No composition defined",
                "Component has no composition defined",
            )

            raise RuntimeError(
                f"Sample `{metadata['blSample']}` with component `{metadata['protein']}` has no composition defined"
            )

        file = f"{metadata['imageDirectory']}/{metadata['fileTemplate']}"
        path = metadata["imageContainerSubPath"]

        datasets = get_datasets(file, path)
        try:
            datasets.first_detector
        except IndexError:
            raise RuntimeError(
                "Could not find any detectors in dataset `{file}::{path}`"
            )

        output_file_name = f"{self.inputs.taskDirectory}/pymca.h5"
        path_without_measurement = path.replace("/measurement", "")
        self.outputs.scan_uri = f"{file}::/{path_without_measurement}"
        self.outputs.detector_name = datasets.first_detector
        self.outputs.output_uri_template = (
            f"{output_file_name}::{path_without_measurement}"
        )

        logger.info("fit output uri: %s", self.outputs.output_uri_template)

        normalize_uris = []
        normalize_reference_values = []
        if datasets.monitor:
            normalize_uris.append(f"{file}::/{path}/{datasets.monitor}")
            normalize_reference_values.append(1)

        if datasets.livetimes:
            normalize_uris.append(f"{file}::/{path}/{datasets.livetimes[0]}")
            normalize_reference_values.append(1)

        logger.info("normalization uris: %s", normalize_uris)

        self.outputs.normalize_uris = normalize_uris
        self.outputs.normalize_reference_values = normalize_reference_values
        self.outputs.norm_output_uri = (
            f"{output_file_name}::{path_without_measurement}/norm"
        )

        configs = []
        # config in session dir
        if metadata.get("sessionDirectory"):
            configs.append(
                os.path.join(metadata["sessionDirectory"], "pymca", "pymca.cfg")
            )

        # global default
        if settings.pymca_config_root:
            configs.append(
                os.path.join(settings.pymca_config_root.format(**metadata), "pymca.cfg")
            )

        cfg = None
        for conf in configs:
            if os.path.exists(conf):
                logger.info(f"Using config: `{conf}`")
                cfg = conf
                break

        if cfg is None:
            add_autoproc_message(
                self.inputs.autoProcProgramId,
                "WARNING",
                "No config found",
                f"Could not find a pymca config file, tried: {configs}",
            )

            raise RuntimeError(
                f"Could not find a pymca config file, tried: `{configs}`"
            )

        self.outputs.config = cfg
        self.outputs.quantification = False


class ParseOutput(
    Task,
    input_names=["data_uri", "metadata", "autoProcProgramId"],
):
    def run(self) -> None:
        logger.debug(f"ParseOutput: {self.inputs.data_uri}")
        h5file, path = self.inputs.data_uri.split("::")
        add_autoproc_attachment(
            self.inputs.autoProcProgramId,
            os.path.dirname(h5file),
            os.path.basename(h5file),
            "result",
        )

        metadata = self.inputs.metadata

        elements = chemparse.parse_formula(metadata["sequence"])
        maps = parse_results(h5file, path, elements)
        norm = "-norm" if "norm" in self.inputs.data_uri else ""
        for map_ in maps:
            map_name = f"{map_.peak[1]}-pymca{norm}"
            mappingROIId = add_mapping_roi(
                startEnergy=0,
                endEnergy=0,
                element=map_.peak[0],
                edge=map_name,
            )

            points = metadata["steps_x"] * metadata["steps_y"]
            add_map(
                xrfFluorescenceMappingROIId=mappingROIId,
                gridInfoId=metadata["gridInfoId"],
                points=points,
                data=map_.data,
                autoProcProgramId=self.inputs.autoProcProgramId,
            )
