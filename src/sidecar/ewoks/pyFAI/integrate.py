import logging
from typing import List

import numpy
import silx
from ewokscore import Task

from ...mimosa.injector import metadata
from ...mimosa.mapping import add_mapping_roi, add_map
from ...mimosa.autoproc import (
    add_autoproc_message,
    get_datacollection_from_autoprocprogram,
)

logger = logging.getLogger(__name__)


def get_last_processed_image_id(result_data_file: str, dataset_path: str) -> int:
    with silx.io.h5py_utils.File(result_data_file) as h5:
        intensity = h5[dataset_path]["intensity"]
        return intensity.shape[0]


def generate_map_data(
    result_data_file: str, dataset_path: str, total_points: int
) -> List:
    with silx.io.h5py_utils.File(result_data_file) as h5:
        intensity = h5[dataset_path]["intensity"]
        pixels = numpy.sum(intensity, axis=1)
        return pixels.tolist() + numpy.full((total_points - len(pixels),), -1).tolist()


class Integrate1dMap(
    Task,
    input_names=["autoProcProgramId", "nxdata_url"],
    output_names=["xrfFluorescenceMappingId"],
):
    def run(self) -> None:
        if not self.inputs.autoProcProgramId:
            print("sidecar: No autoProcProgramId, not processing")
            return

        autoProcProgramId = self.inputs.autoProcProgramId
        dataCollectionId = get_datacollection_from_autoprocprogram(autoProcProgramId)
        dc_metadata = metadata(dataCollectionId)
        if not dc_metadata["gridInfoId"]:
            print("sidecar: No gridInfoId, not processing")
            return

        total_points = dc_metadata["steps_x"] * dc_metadata["steps_y"]
        result_file, dataset_path = self.inputs.nxdata_url.split("::")

        mappingROIId = add_mapping_roi(startEnergy=0, endEnergy=0, scalar="pyFAI")

        try:
            latest_image_id = get_last_processed_image_id(
                result_data_file=result_file, dataset_path=dataset_path
            )
            add_autoproc_message(
                autoProcProgramId,
                "INFO",
                "Processing Ended",
                f"{latest_image_id} images processed",
            )
        except Exception as e:
            print("sidecar: Could not get last image", str(e))

        map_data = generate_map_data(
            result_data_file=result_file,
            dataset_path=dataset_path,
            total_points=total_points,
        )

        self.outputs.xrfFluorescenceMappingId = add_map(
            xrfFluorescenceMappingROIId=mappingROIId,
            gridInfoId=dc_metadata["gridInfoId"],
            points=total_points,
            data=map_data,
            autoProcProgramId=autoProcProgramId,
        )
