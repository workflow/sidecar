import logging

from ewokscore import Task

logger = logging.getLogger(__name__)


SIDECAR_INPUTS = [
    "metadata",
    "taskDirectory",
    "dataCollectionId",
    "processingJobId",
    "autoProcProgramId",
]


class Inputs(
    Task,
    input_names=SIDECAR_INPUTS,
    output_names=SIDECAR_INPUTS,
):
    """Map sidecars inputs to a task so they can be redirected"""

    def run(self) -> None:
        for input_name in SIDECAR_INPUTS:
            setattr(self.outputs, input_name, getattr(self.inputs, input_name))
