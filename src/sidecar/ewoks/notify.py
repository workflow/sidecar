import logging

from ewokscore import Task
from ..kombu.notify import notify

logger = logging.getLogger(__name__)


class NotifyBeamline(
    Task,
    input_names=["beamline", "message"],
):
    def run(self) -> None:
        if not isinstance(self.inputs.message, dict):
            raise AttributeError(f"`{self.inputs.message}` should be a dict")

        notify(self.inputs.beamline, self.inputs.message)
