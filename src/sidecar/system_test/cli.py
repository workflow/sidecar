import argparse
import logging

from .base import SystemTest, load_config


logging.basicConfig(level=logging.INFO)


def run(args=None) -> None:
    parser = argparse.ArgumentParser(usage="sidecar.system_test")
    parser.add_argument(
        "-c",
        dest="classes",
        help="Filter tests to specific classes",
        action="append",
    )
    parser.add_argument(
        "-k",
        dest="tests",
        help="Filter tests to specific tests",
        action="append",
    )
    parser.add_argument(
        "-o", "--output", help="Output junit xml name", default="output.xml"
    )
    parser.add_argument("-d", "--debug", action="store_true", help="Enable debugging")
    args = parser.parse_args(args)

    config = load_config()
    system_test = SystemTest(config, debug=args.debug)
    system_test.run(
        class_filter=args.classes, test_filter=args.tests, output_xml=args.output
    )
