import logging
import json
import os
import pkg_resources
import traceback
from typing import Any, Callable, Optional, Type
from pydantic import BaseModel, ValidationError

import ruamel.yaml
import junit_xml

from .result import Result
from ..settings.server import settings


logger = logging.getLogger("SystemTest")


class ConfigSchema(BaseModel):
    class_filter: Optional[list[str]] = None

    class Config:
        extra = "allow"


def load_config() -> dict[str, Any]:
    if not settings.system_test_config:
        raise RuntimeError("`SYSTEM_TEST_CONFIG` is not defined")

    config_file_path = settings.system_test_config
    if not os.path.exists(config_file_path):
        raise RuntimeError(f"`SYSTEM_TEST_CONFIG={config_file_path}` does not exist")

    with open(config_file_path) as config_file:
        yaml = ruamel.yaml.YAML()
        contents: dict[str, Any] = yaml.load(config_file)  # nosec
        return ConfigSchema(**contents).model_dump()


class BaseSystemTest:
    schema: Optional[Type[BaseModel]] = None

    def __init__(self, config: dict[str, Any]) -> None:
        self._config = config

        if self.schema:
            try:
                self._local_config = self.schema(
                    **self._config.get(self.__class__.__name__, {})
                ).model_dump()
            except ValidationError as ex:
                print(f"There were errors validating the config for `{self.name}`:")
                errors = json.loads(ex.json())
                for error in errors:
                    print(f"  {error['msg']}: {','.join(error['loc'])}")

                exit(1)

    @property
    def enumerated_test_functions(self) -> list[Callable[[], None]]:
        """Returns functions on the class starting with test_*"""
        return [
            getattr(self, function)
            for function in dir(self)
            if function.startswith("test_")
        ]

    @property
    def name(self) -> str:
        return self.__class__.__name__

    @property
    def config(self) -> dict[str, Any]:
        return self._local_config


class CollectedTest:
    """A BaseSystemTest and an associated Result"""

    def __init__(self, test: Callable[[], None]) -> None:
        self._test = test
        test_self = getattr(test, "__self__")
        self._result = Result(
            test.__name__,
            test_self.__class__.__name__,
        )

    def __repr__(self) -> str:
        return f"<{self.name}: Result: {self._result}>"

    @property
    def name(self) -> str:
        test_self = getattr(self._test, "__self__")
        return f"{test_self.__class__.__name__}.{self._test.__name__}"

    @property
    def result(self) -> Result:
        return self._result

    @property
    def success(self) -> bool:
        return self.result.success

    @property
    def failure(self) -> bool:
        return self.result.failure

    def execute(self) -> None:
        try:
            self._result.start()
            self._test()
        except Exception as ex:
            trace = traceback.format_exc()
            self._result.exception = str(ex)
            self._result.traceback = trace
            self._result.set_failure()
        else:
            self._result.set_success()


class SystemTest:
    """Run the actual tests"""

    def __init__(self, config: dict[str, Any], *, debug: bool = False) -> None:
        self._debug = debug
        self._config = config

    def collect(
        self,
        class_filter: Optional[list[str]] = None,
        test_filter: Optional[list[str]] = None,
    ) -> list[CollectedTest]:
        test_functions: list[CollectedTest] = []
        test_classes: list[BaseSystemTest] = []
        for entry in pkg_resources.iter_entry_points("sidecar.system_tests"):
            cls = entry.load()
            instance = cls(self._config)
            test_classes.append(instance)

        if not class_filter:
            class_filter = self._config.get("class_filter")

        filtered_classes = []
        for test_class in test_classes:
            if class_filter:
                if any(
                    test_class.name.lower().startswith(selected_class.lower())
                    for selected_class in class_filter
                ):
                    filtered_classes.append(test_class)

        if class_filter:
            filter_type = (
                "config"
                if self._config.get("class_filter")
                else "command line arguments"
            )
            logger.info(
                f"Filtered out {(len(test_classes) - len(filtered_classes))} class(es) via {filter_type}"
            )
            test_classes = filtered_classes

        total_tests = 0
        for test_class in test_classes:
            logger.debug(f"Collecting tests from {test_class.name}")
            total_tests += len(test_class.enumerated_test_functions)
            for test in test_class.enumerated_test_functions:
                if test_filter is None or (
                    any(
                        test.__name__.lower().startswith(selected_test.lower())
                        for selected_test in test_filter
                    )
                ):
                    test_functions.append(CollectedTest(test))

        if test_filter:
            logger.info(
                f"Filtered {total_tests - len(test_functions)} test(s) via command line arguments"
            )
            test_classes = filtered_classes

        logger.info(f"Collected {len(test_classes)} test class(es)")
        logger.info(f"Collected {len(test_functions)} test(s)")

        return test_functions

    def run(
        self,
        *,
        class_filter: Optional[list[str]] = None,
        test_filter: Optional[list[str]] = None,
        output_xml: str,
        raise_on_error: bool = False,
    ) -> None:
        tests = self.collect(class_filter=class_filter, test_filter=test_filter)
        for test in tests:
            logger.info(f" - Running: {test.name}")
            test.execute()

        test_suite = junit_xml.TestSuite(
            "sidecar.system_test", [test.result.as_junit() for test in tests]
        )
        with open(output_xml, "w") as output_file:
            junit_xml.to_xml_report_file(output_file, [test_suite], prettyprint=True)

        successes = sum(test.success for test in tests)
        logger.info(
            f"System test run completed, {successes} of {len(tests)} tests succeeded."
        )

        for test in tests:
            if test.failure:
                if self._debug:
                    logger.error(
                        f"  {test.result.group} {test.result.name} failed:\n    {test.result.traceback}",
                    )
                else:
                    logger.error(
                        f"  {test.result.group} {test.result.name} failed:\n    {test.result.exception}",
                    )

        if successes < len(tests):
            if raise_on_error:
                raise RuntimeError("System test failed")
            else:
                exit(1)
