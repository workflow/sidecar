from enum import IntEnum
from dataclasses import dataclass
from datetime import datetime
import timeit
from typing import Optional

from junit_xml import TestCase


class Status(IntEnum):
    SUCCESS = 1
    FAIL = 0


@dataclass
class Result:
    name: str
    group: str
    status: Optional[Status] = None
    time: Optional[datetime] = None
    duration: Optional[float] = None
    exception: Optional[str] = None
    traceback: Optional[str] = None

    def start(self) -> None:
        self.time = datetime.now()
        self._start_time = timeit.default_timer()

    def update_timer(self) -> None:
        self.duration = timeit.default_timer() - self._start_time

    def set_success(self) -> None:
        self.update_timer()
        self.status = Status.SUCCESS

    def set_failure(self) -> None:
        self.update_timer()
        self.status = Status.FAIL

    @property
    def success(self) -> bool:
        return self.status == Status.SUCCESS

    @property
    def failure(self) -> bool:
        return self.status == Status.FAIL

    def as_junit(self) -> TestCase:
        jxml = TestCase(self.name, self.group, self.duration)
        if self.status == Status.FAIL:
            jxml.add_failure_info(message=f"{self.exception}\n{self.traceback}")
        return jxml
