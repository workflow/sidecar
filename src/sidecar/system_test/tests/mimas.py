from pydantic import BaseModel

from ..base import BaseSystemTest


class MimasSchema(BaseModel):
    start_datacollection: int
    start_invocations: list[str]
    end_datacollection: int
    end_invocations: list[str]
    timeout: int = 5


class Mimas(BaseSystemTest):
    schema = MimasSchema

    def test_mimas(self) -> None:
        return
        for event in ["start", "end"]:
            result = None
            if result != self.config[f"{event}_invocations"]:
                raise RuntimeError(
                    f"Unexpected result:\n      {result}\n    is not equal to:\n      {self.config[f'{event}_invocations']}"
                )
