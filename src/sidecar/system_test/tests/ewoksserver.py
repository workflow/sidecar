from pydantic import BaseModel

from ..base import BaseSystemTest
from ...server.ewoksserver import get_workflows


class EwoksServerSchema(BaseModel):
    pass


class EwoksServer(BaseSystemTest):
    schema = EwoksServerSchema

    def test_ewoksserver(self) -> None:
        get_workflows()
