import uuid

from pydantic import BaseModel

from ..base import BaseSystemTest
from ...test_utils import consume
from ...kombu.message import send_message
from ...server.messages import WorkflowMessage


class ServerSchema(BaseModel):
    pass


class Server(BaseSystemTest):
    schema = ServerSchema

    def test_server(self) -> None:
        beamline = "system_test"
        message = {"test": 123, "uuid": str(uuid.uuid4())}
        send_message(
            WorkflowMessage(
                workflow="test-notify",
                dataCollectionId=1,
                mimosa_register=False,
                parameters={"message": message, "beamline": beamline},
            )
        )

        consume(beamline, message)
