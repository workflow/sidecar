import uuid

from pydantic import BaseModel

from ..base import BaseSystemTest
from ...test_utils import consume
from ...kombu.notify import notify


class KombuSchema(BaseModel):
    pass


class Kombu(BaseSystemTest):
    schema = KombuSchema

    def test_kombu(self) -> None:
        beamline = "system_test"
        message = {"test": 123, "uuid": str(uuid.uuid4())}
        notify(beamline, message)
        consume(beamline, message)
