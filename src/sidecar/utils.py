import json
import gzip
from typing import List, Optional


from pint import UnitRegistry

ureg = UnitRegistry()


def to_energy(wavelength: float, unit: Optional[str] = None) -> Optional[float]:
    if wavelength is None:
        return None

    pint_unit = ureg.keV if unit == "k" else ureg.eV
    energy: float = (
        ((ureg.planck_constant * ureg.c) / (wavelength * ureg.angstrom))
        .to(pint_unit)
        .magnitude
    )
    return energy


def gzip_json(obj: List[float]) -> bytes:
    """dump an array to json and gzip"""
    return gzip.compress(json.dumps(obj).encode())
