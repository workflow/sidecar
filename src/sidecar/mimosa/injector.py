import os
from typing import Any, List, Optional
import sqlalchemy
from sqlalchemy.sql.expression import cast
from sqlalchemy import func
import logging

from . import models

logger = logging.getLogger(__name__)


def scan_processed_directory(filename: str) -> str:
    """TODO: This could move into blissoda.utils.directories"""
    from blissoda.utils import directories

    root = directories.get_processed_dir(filename)
    collection = os.path.basename(directories.get_collection_dir(filename))
    dataset = os.path.basename(directories.get_dataset_dir(filename))
    return os.path.join(root, collection, dataset)


def get_paths(image_directory: str) -> List[str]:
    """Determine the root session and the processing output directories

    Delegate to `blissoda` to determine these paths
    """
    from blissoda.utils import directories

    dataset_file = f"{image_directory}/file.h5"

    working_directory = scan_processed_directory(dataset_file)
    session_directory = directories.get_session_dir(dataset_file)
    directory_version = directories.get_directory_version(dataset_file)

    logger.debug(f"directory version `{directory_version}`")
    logger.debug(f"image directory `{dataset_file}`")
    logger.debug(f"interpolated session `{session_directory}`")
    logger.debug(f"interpolated working `{working_directory}`")
    return session_directory, working_directory


def get_datacollection(dataCollectionId: int):
    from .session import get_session

    with get_session() as ses:
        datacollection: Optional[models.DataCollection] = (
            ses.query(
                models.DataCollection.fileTemplate,
                models.DataCollection.imageDirectory,
                models.DataCollection.imageContainerSubPath,
                models.DataCollection.numberOfImages,
                models.DataCollection.exposureTime,
                models.DataCollection.runStatus,
                models.DataCollection.wavelength,
                # DataCollectionGroup
                models.DataCollectionGroup.experimentType,
                models.DataCollectionGroup.blSampleId,
                # Session
                models.BLSession.beamLineName,
                models.BLSession.startDate.label("sessionStartDate"),
                func.concat(
                    models.Proposal.proposalCode,
                    models.Proposal.proposalNumber,
                    "-",
                    models.BLSession.visit_number,
                ).label("session"),
                # Proposal
                func.concat(
                    models.Proposal.proposalCode, models.Proposal.proposalNumber
                ).label("proposal"),
                # BLSample
                models.BLSample.name.label("blSample"),
                # Protein
                models.Protein.sequence,
                models.Protein.proteinId,
                models.Protein.acronym.label("protein"),
                # GridInfo
                models.GridInfo.gridInfoId,
                cast(models.GridInfo.steps_x, sqlalchemy.Integer).label("steps_x"),
                cast(models.GridInfo.steps_y, sqlalchemy.Integer).label("steps_y"),
            )
            .join(models.DataCollection.DataCollectionGroup)
            .outerjoin(
                models.BLSample,
                models.BLSample.blSampleId == models.DataCollectionGroup.blSampleId,
            )
            .outerjoin(
                models.Crystal, models.Crystal.crystalId == models.BLSample.crystalId
            )
            .outerjoin(
                models.Protein, models.Protein.proteinId == models.Crystal.proteinId
            )
            .outerjoin(
                models.GridInfo,
                models.GridInfo.dataCollectionId
                == models.DataCollection.dataCollectionId,
            )
            .join(
                models.BLSession,
                models.BLSession.sessionId == models.DataCollectionGroup.sessionId,
            )
            .join(
                models.Proposal,
                models.Proposal.proposalId == models.BLSession.proposalId,
            )
            .filter(models.DataCollection.dataCollectionId == dataCollectionId)
            .first()
        )

        if not datacollection:
            raise RuntimeError(f"No such datacollection {dataCollectionId}")

        return datacollection


def metadata(dataCollectionId: int) -> dict[str, Any]:
    datacollection = get_datacollection(dataCollectionId)

    dict_result = datacollection._asdict()
    dict_result["beamLineNameLower"] = dict_result["beamLineName"].lower()

    session_directory, working_directory = get_paths(datacollection.imageDirectory)
    dict_result["sessionStartDate"] = dict_result["sessionStartDate"].strftime("%Y%m%d")
    dict_result["workingDirectory"] = working_directory
    dict_result["sessionDirectory"] = session_directory

    return dict_result
