from contextlib import contextmanager
from typing import Generator, Any
import sqlalchemy
import sqlalchemy.orm
import sqlalchemy.schema
import urllib.parse

from ..settings.mimosa import settings


engine = sqlalchemy.create_engine(
    url=f"mysql+mysqlconnector://{settings.mimosa_database_user}:{urllib.parse.quote(settings.mimosa_database_password)}@{settings.mimosa_database_url}",
    # Blobs get decoded as str without this resulting in TypeError: string argument without an encoding
    # https://stackoverflow.com/a/53468522
    connect_args={"use_pure": True},  # type: ignore
    isolation_level="READ UNCOMMITTED",
    # https://docs.sqlalchemy.org/en/13/core/pooling.html#dealing-with-disconnects
    pool_pre_ping=True,
    pool_recycle=3600,
    # pooling
    # https://docs.sqlalchemy.org/en/13/errors.html#error-3o7r
    # maybe consider https://docs.sqlalchemy.org/en/13/core/pooling.html#sqlalchemy.pool.NullPool ?
    pool_size=settings.mimosa_database_pool,
    max_overflow=settings.mimosa_database_overflow,
)

_session = sqlalchemy.orm.sessionmaker(autocommit=False, autoflush=False, bind=engine)


@contextmanager
def get_session() -> Generator[sqlalchemy.orm.Session, Any, None]:
    session = _session()
    try:
        yield session
        session.commit()
    except:  # noqa
        session.rollback()
        raise
    finally:
        session.close()
