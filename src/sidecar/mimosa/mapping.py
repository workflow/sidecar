from typing import List, Optional

from . import models
from ..utils import gzip_json


def get_mapping_rois(blSampleId: int) -> List[models.XRFFluorescenceMappingROI]:
    from .session import get_session

    with get_session() as session:
        query = session.query(models.XRFFluorescenceMappingROI).filter(
            models.XRFFluorescenceMappingROI.blSampleId == blSampleId
        )

        rois = query.all()
        if rois:
            for roi in rois:
                session.expunge(roi)

        return rois


def add_mapping_roi(
    startEnergy: float,
    endEnergy: float,
    element: Optional[str] = None,
    edge: Optional[str] = None,
    scalar: Optional[str] = None,
    blSampleId: Optional[int] = None,
) -> int:
    from .session import get_session

    with get_session() as session:
        mappingROI = models.XRFFluorescenceMappingROI(
            startEnergy=startEnergy,
            endEnergy=endEnergy,
            element=element,
            edge=edge,
            scalar=scalar,
            blSampleId=blSampleId,
        )

        session.add(mappingROI)
        session.commit()

        return mappingROI.xrfFluorescenceMappingROIId


def get_maps(dataCollectionId: int) -> List[models.XRFFluorescenceMapping]:
    from .session import get_session

    with get_session() as session:
        query = (
            session.query(models.XRFFluorescenceMapping)
            .join(models.GridInfo)
            .filter(models.GridInfo.dataCollectionId == dataCollectionId)
        )

        maps = query.all()
        if maps:
            for map_ in maps:
                session.expunge(map_.XRFFluorescenceMappingROI)
                session.expunge(map_)

        return maps


def add_map(
    xrfFluorescenceMappingROIId: int,
    gridInfoId: int,
    data: List[float],
    dataFormat: str = "json+gzip",
    points: Optional[int] = None,
    autoProcProgramId: Optional[int] = None,
) -> int:
    from .session import get_session

    with get_session() as session:
        map = models.XRFFluorescenceMapping(
            xrfFluorescenceMappingROIId=xrfFluorescenceMappingROIId,
            gridInfoId=gridInfoId,
            dataFormat=dataFormat,
            data=gzip_json(data),
            points=points,
            autoProcProgramId=autoProcProgramId,
        )

        session.add(map)
        session.commit()

        return map.xrfFluorescenceMappingId
