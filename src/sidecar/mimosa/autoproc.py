from datetime import datetime, timedelta
import json
from typing import Any, List, Optional
from sqlalchemy import func
from sqlalchemy.orm import joinedload

from . import models


def create_processing_job(values: dict[str, Any]) -> list[int]:
    from .session import get_session

    with get_session() as session:
        processingJob = models.ProcessingJob(
            dataCollectionId=values["dataCollectionId"],
            recipe=values["recipe"],
            automatic=values["automatic"],
            displayName=values.get("displayName", values["recipe"]),
        )

        session.add(processingJob)
        session.commit()

        if values.get("parameters"):
            add_processing_job_parameters(
                processingJob.processingJobId, values["parameters"]
            )

        autoProcProgramId = create_autoprocprogram(
            {**values, "processingJobId": processingJob.processingJobId}
        )

        return (processingJob.processingJobId, autoProcProgramId)


def create_autoprocprogram(values: dict[str, Any]) -> int:
    from .session import get_session

    with get_session() as session:
        autoProcProgram = models.AutoProcProgram(
            processingJobId=values["processingJobId"],
            processingPrograms=values["recipe"],
            processingCommandLine=values.get("processingCommandLine", values["recipe"]),
            processingEnvironment=values.get("processingEnvironment"),
            recordTimeStamp=func.now(),
        )
        session.add(autoProcProgram)
        session.commit()

        return autoProcProgram.autoProcProgramId


def get_datacollection_from_autoprocprogram(autoProcProgramId: int) -> int:
    from .session import get_session

    with get_session() as session:
        result = (
            session.query(models.ProcessingJob.dataCollectionId)
            .join(
                models.AutoProcProgram,
                models.ProcessingJob.processingJobId
                == models.AutoProcProgram.processingJobId,
            )
            .filter(models.AutoProcProgram.autoProcProgramId == autoProcProgramId)
            .first()
        )
        result = result._asdict()
        return result.get("dataCollectionId")


def retrieve_processing_job(processingJobId: int) -> Optional[models.ProcessingJob]:
    from .session import get_session

    with get_session() as session:
        processingJob: Optional[models.ProcessingJob] = (
            session.query(models.ProcessingJob)
            .options(joinedload(models.ProcessingJob.ProcessingJobParameters))
            .filter(models.ProcessingJob.processingJobId == processingJobId)
            .first()
        )

        if processingJob:
            for p in processingJob.ProcessingJobParameters:
                session.expunge(p)
            session.expunge(processingJob)
            return processingJob

    return None


def retrieve_processing_job_beamline(processingJobId: int) -> Optional[str]:
    from .session import get_session

    with get_session() as session:
        blSession: Optional[models.BLSession] = (
            session.query(models.BLSession)
            .join(models.DataCollectionGroup)
            .join(models.DataCollection)
            .join(models.ProcessingJob)
            .filter(models.ProcessingJob.processingJobId == processingJobId)
            .first()
        )

        if blSession:
            session.expunge(blSession)
            return blSession.beamLineName

    return None


def get_autoproc_program(processingJobId: int) -> Optional[models.AutoProcProgram]:
    from .session import get_session

    with get_session() as session:
        autoProcProgram: Optional[models.AutoProcProgram] = (
            session.query(models.AutoProcProgram)
            .filter(models.AutoProcProgram.processingJobId == processingJobId)
            .first()
        )

        if autoProcProgram:
            session.expunge(autoProcProgram)
            return autoProcProgram

    return None


def get_processing_jobs(since_days: Optional[int] = None) -> List[models.ProcessingJob]:
    from .session import get_session

    with get_session() as session:
        query = (
            session.query(models.ProcessingJob)
            .options(joinedload(models.ProcessingJob.ProcessingJobParameters))
            .join(
                models.AutoProcProgram,
                models.ProcessingJob.processingJobId
                == models.AutoProcProgram.processingJobId,
            )
            .options(joinedload(models.ProcessingJob.AutoProcProgram))
            .filter(models.AutoProcProgram.processingStatus == None)  # noqa: E711
        )

        if since_days:
            query = query.filter(
                models.ProcessingJob.recordTimestamp
                > (datetime.now() + timedelta(days=since_days))
            )

        processingJobs: List[models.ProcessingJob] = query.all()
        for processingJob in processingJobs:
            for p in processingJob.ProcessingJobParameters:
                session.expunge(p)
            session.expunge(processingJob)
            for ap in processingJob.AutoProcProgram:
                session.expunge(ap)

        return processingJobs


def get_processing_parameter(
    parameterKey: str,
    processingJobParameters: List[models.ProcessingJobParameter],
) -> Optional[str]:
    for param in processingJobParameters:
        if param.parameterKey == parameterKey:
            return param.parameterValue


def add_processing_job_parameters(processingJobId: int, values: dict[str, Any]) -> None:
    from .session import get_session

    with get_session() as session:
        for key, value in values.items():
            safe_value = value
            if isinstance(safe_value, dict) or isinstance(safe_value, list):
                safe_value = json.dumps(safe_value)
            processingJobParameter = models.ProcessingJobParameter(
                processingJobId=processingJobId,
                parameterKey=key,
                parameterValue=safe_value,
            )
            session.add(processingJobParameter)
            session.commit()


def update_processing_job_parameters(
    processingJobId: int, updatedParameters: dict[str, Any]
) -> None:
    from .session import get_session

    with get_session() as session:
        processingJobParameters = (
            session.query(models.ProcessingJobParameter)
            .filter(models.ProcessingJobParameter.processingJobId == processingJobId)
            .all()
        )
        for parameterKey, parameterValue in updatedParameters.items():
            for existing in processingJobParameters:
                if existing.parameterKey == parameterKey:
                    existing.parameterValue = parameterValue
                    break
            else:
                processingJobParameter = models.ProcessingJobParameter(
                    processingJobId=processingJobId,
                    parameterKey=parameterKey,
                    parameterValue=parameterValue,
                )
                session.add(processingJobParameter)

        session.commit()


def update_autoproc_program(autoProcProgramId: int, values: dict[str, Any]) -> bool:
    from .session import get_session

    with get_session() as session:
        autoProcProgram: Optional[models.AutoProcProgram] = (
            session.query(models.AutoProcProgram)
            .filter(models.AutoProcProgram.autoProcProgramId == autoProcProgramId)
            .first()
        )

        if not autoProcProgram:
            raise KeyError(f"No such autoProcProgram {autoProcProgramId}")

        processingStartTime = values.pop("processingStartTime", None)
        if processingStartTime:
            if processingStartTime == "NULL":
                autoProcProgram.processingStartTime = None
            else:
                autoProcProgram.processingStartTime = func.now()

        processingEndTime = values.pop("processingEndTime", None)
        if processingEndTime:
            if processingEndTime == "NULL":
                autoProcProgram.processingEndTime = None
            else:
                autoProcProgram.processingEndTime = func.now()

        for key, value in values.items():
            setattr(autoProcProgram, key, value)

        session.commit()

        return True


def get_autoproc_attachments(
    *,
    autoProcProgramId: Optional[int] = None,
    autoProcProgramAttachmentId: Optional[int] = None,
) -> list[models.AutoProcProgramAttachment]:
    from .session import get_session

    with get_session() as session:
        query = session.query(models.AutoProcProgramAttachment)

        if autoProcProgramId:
            query = query.filter(
                models.AutoProcProgramAttachment.autoProcProgramId == autoProcProgramId
            )

        if autoProcProgramAttachmentId:
            query = query.filter(
                models.AutoProcProgramAttachment.autoProcProgramAttachmentId
                == autoProcProgramAttachmentId
            )

        attachemnts = query.all()
        if attachemnts:
            for attachment in attachemnts:
                session.expunge(attachment)

        return attachemnts


def update_autoproc_attachment(
    autoProcProgramAttachmentId: int,
    **kwargs,
) -> Optional[models.AutoProcProgramAttachment]:
    from .session import get_session

    attachments = get_autoproc_attachments(
        autoProcProgramAttachmentId=autoProcProgramAttachmentId
    )

    if attachments:
        with get_session() as session:
            attachment = attachments[0]
            for k in ["filePath", "fileType", "fileName", "importanceRank"]:
                if k in kwargs:
                    setattr(attachment, k, kwargs[k])

            session.commit()


def add_autoproc_attachment(
    autoProcProgramId: int,
    filePath: str,
    fileName: str,
    fileType: str,
    importanceRank: int = 1,
    createOrUpdate: bool = False,
) -> int:
    from .session import get_session

    with get_session() as session:
        if createOrUpdate:
            attachments: Optional[List[models.AutoProcProgramAttachment]] = (
                session.query(models.AutoProcProgramAttachment)
                .filter(
                    models.AutoProcProgramAttachment.autoProcProgramId
                    == autoProcProgramId,
                )
                .filter(models.AutoProcProgramAttachment.fileType == fileType)
                .all()
            )

            # Overwrite the first attachment of the same type
            if attachments:
                attachment = attachments[0]
                attachment.filePath = filePath
                attachment.fileName = fileName
                attachment.recordTimeStamp = func.now()

                session.commit()

                return attachment.autoProcProgramAttachmentId

        autoProcProgramAttachment = models.AutoProcProgramAttachment(
            autoProcProgramId=autoProcProgramId,
            filePath=filePath,
            fileName=fileName,
            fileType=fileType,
            importanceRank=importanceRank,
            recordTimeStamp=func.now(),
        )

        session.add(autoProcProgramAttachment)
        session.commit()

        return autoProcProgramAttachment.autoProcProgramAttachmentId


def add_autoproc_message(
    autoProcProgramId: int, severity: str, message: str, description: str
) -> int:
    from .session import get_session

    with get_session() as session:
        autoProcProgramMessage = models.AutoProcProgramMessage(
            autoProcProgramId=autoProcProgramId,
            message=message,
            description=description,
            severity=severity,
            recordTimeStamp=func.now(),
        )

        session.add(autoProcProgramMessage)
        session.commit()

        return autoProcProgramMessage.autoProcProgramMessageId
