import logging

import graypy
import graypy.handler

from .. import __version__
from ..settings.server import ServerSettings


class GrayPyFilter(logging.Filter):
    """Inject some useful metadata into graylog records"""

    def filter(self, record):
        record.sidecar_version = __version__
        record.process_name = "sidecar"
        return True


def init_graylog():
    settings = ServerSettings()

    class PythonLevelToSyslogConverter:
        @staticmethod
        def get(level, _):
            if level < 20:
                return 7  # DEBUG
            elif level < 25:
                return 6  # INFO
            elif level < 30:
                return 5  # NOTICE
            elif level < 40:
                return 4  # WARNING
            elif level < 50:
                return 3  # ERROR
            elif level < 60:
                return 2  # CRITICAL
            else:
                return 1  # ALERT

    graypy.handler.SYSLOG_LEVELS = PythonLevelToSyslogConverter()

    handler = graypy.GELFUDPHandler
    if settings.graylog_url:
        host, port = settings.graylog_url.split(":")
        if host and port:
            graylog = handler(host, int(port), level_names=True)
            graylog.addFilter(GrayPyFilter())
            logging.getLogger().addHandler(graylog)
