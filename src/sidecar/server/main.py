import logging
import threading
import time

from kombu import Connection, Queue

from ..settings.server import settings
from .execute import check_state, execute
from ..mimas.base import execute as mimas_execute
from .messages import parse_message, WorkflowMessage, MimasMessage
from .logging import init_graylog

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class SidecarServer:
    def __init__(self):
        init_graylog()

        self._status_thread_running = True
        self._status_thread = threading.Thread(target=self.check_task_status)
        self._status_thread.start()

    def shutdown(self):
        logger.info("Shutting down status thread")
        self._status_thread_running = False
        logger.info("Waiting for thread exit")
        self._status_thread.join()

    def run(self):
        with Connection(settings.broker_url) as conn:
            logger.info(f"Subscribing to queue {settings.broker_queue}")
            queue = Queue(
                settings.broker_queue,
                durable=True,
            )
            while True:
                with conn.SimpleBuffer(queue) as simple_queue:
                    message = simple_queue.get(block=True)
                    message_type = parse_message(message.headers, message.payload)
                    if message_type:
                        if isinstance(message_type, WorkflowMessage):
                            execute(**message_type.__dict__)
                        elif isinstance(message_type, MimasMessage):
                            invocations = mimas_execute(
                                message_type.event, message_type.dataCollectionId
                            )
                            if invocations:
                                logger.info(
                                    f"Mimas will invoke: `{','.join([invocation.graph for invocation in invocations])}` for data collection `{message_type.dataCollectionId}` with event `{message_type.event}`"
                                )
                                for invocation in invocations:
                                    execute(
                                        invocation.graph,
                                        dataCollectionId=message_type.dataCollectionId,
                                    )
                            else:
                                logger.info(
                                    f"Mimas: nothing to invoke for data collection `{message_type.dataCollectionId}` with event `{message_type.event}`"
                                )
                    message.ack()

    def check_task_status(self):
        logger.info("Starting status thread")
        while self._status_thread_running:
            check_state()
            time.sleep(5)


def main():
    server = SidecarServer()
    try:
        server.run()
    except KeyboardInterrupt:
        server.shutdown()
