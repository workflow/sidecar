import requests
from typing import Any, Optional

from ..settings.server import settings


def get_workflows():
    try:
        resp = requests.get(
            f"{settings.ewoks_server_url}/api/workflows",
            timeout=5,
        )
    except requests.exceptions.ConnectionError:
        raise RuntimeError(
            f"Could not connect to ewoksserver {settings.ewoks_server_url}"
        )

    if resp.status_code != 200:
        raise RuntimeError("Could not get workflow list")

    return resp.json()


def start_workflow(
    workflow: str,
    execute_arguments: Optional[dict[str, Any]] = {},
    worker_options: Optional[dict[str, Any]] = {},
) -> str:
    try:
        resp = requests.post(
            f"{settings.ewoks_server_url}/api/execute/{workflow}",
            json={
                "execute_arguments": execute_arguments,
                "worker_options": worker_options,
            },
            timeout=5,
        )
    except requests.exceptions.ConnectionError:
        raise RuntimeError(
            f"Could not connect to ewoksserver {settings.ewoks_server_url}"
        )

    try:
        json_response = resp.json()
    except Exception:
        json_response = {}

    if resp.status_code != 200:
        print(resp.status_code, json_response)
        if isinstance(json_response, dict):
            message = json_response.get("message", json_response)
        else:
            message = json_response
        raise RuntimeError(f"Could not start workflow: {message}")

    return json_response["job_id"]


def get_workflow_events(
    ewoks_job_id: str,
    event_type: Optional[str] = None,
    event_context: Optional[str] = None,
):
    params = {"job_id": ewoks_job_id}
    if event_type:
        params["type"] = event_type

    if event_context:
        params["context"] = event_context

    try:
        resp = requests.get(
            f"{settings.ewoks_server_url}/api/execution/events",
            params=params,
            timeout=5,
        )
    except requests.exceptions.ConnectionError:
        raise RuntimeError(
            f"Could not connect to ewoksserver {settings.ewoks_server_url}"
        )

    try:
        json_response = resp.json()
    except Exception:
        json_response = {}

    if isinstance(json_response, dict):
        message = json_response.get("message", json_response)
    else:
        message = json_response

    if resp.status_code != 200:
        raise RuntimeError(f"Could not get workflow events: {message}")

    return json_response
