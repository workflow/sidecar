from datetime import datetime
import json
import logging
import os
import traceback
from typing import Any, Optional
import uuid

from .ewoksserver import start_workflow, get_workflow_events

logger = logging.getLogger(__name__)


def execute(
    workflow: str,
    *,
    dataCollectionId: int,
    automatic: bool = True,
    parameters: dict[str, Any] = {},
    mimosa_register: bool = True,
):
    from ..mimosa.autoproc import (
        create_processing_job,
        update_autoproc_program,
        update_processing_job_parameters,
    )
    from ..mimosa import metadata as get_metadata

    job_uuid = str(uuid.uuid4())
    if mimosa_register:
        processingJobId, autoProcProgramId = create_processing_job(
            {
                "dataCollectionId": dataCollectionId,
                "recipe": os.path.basename(workflow).replace(".json", ""),
                "automatic": automatic,
                "parameters": {"uuid": job_uuid, **parameters},
            }
        )
        logger.info(
            f"Created processingJobId {processingJobId} and autoProcProgramId {autoProcProgramId} for workflow {workflow} from dataCollectionId: {dataCollectionId}"
        )
    else:
        processingJobId = None
        autoProcProgramId = None

    metadata = get_metadata(dataCollectionId)
    date_string = datetime.now().strftime("%Y-%m-%dT%Hh%Mm%S")
    taskDirectory = (
        f"{metadata['workingDirectory']}/{date_string}-{job_uuid}-{workflow}"
    )
    logger.debug(f"Task Directory: {taskDirectory}")

    inputs = [
        {"name": "metadata", "value": metadata},
        {"name": "taskDirectory", "value": taskDirectory},
        {"name": "dataCollectionId", "value": dataCollectionId},
        {"name": "processingJobId", "value": processingJobId},
        {"name": "autoProcProgramId", "value": autoProcProgramId},
    ]

    for key, value in parameters.items():
        inputs.append({"name": key, "value": value})

    if mimosa_register and autoProcProgramId:
        update_autoproc_program(
            autoProcProgramId,
            {
                "processingStartTime": True,
                "processingMessage": "processing started",
            },
        )

    try:
        ewoks_job_id = start_workflow(workflow, execute_arguments={"inputs": inputs})
        if mimosa_register and autoProcProgramId:
            update_processing_job_parameters(
                processingJobId=processingJobId,
                updatedParameters={
                    "ewoks_job_id": ewoks_job_id,
                    "taskDirectory": taskDirectory,
                },
            )
    except Exception as e:
        logger.exception("Execute workflow failed")
        if mimosa_register and autoProcProgramId:
            update_autoproc_program(
                autoProcProgramId,
                {
                    "processingEndTime": True,
                    "processingStatus": 0,
                    "processingMessage": "processing failed",
                },
            )

            tb = "".join(traceback.format_tb(e.__traceback__))
            attach_and_save_log(
                autoProcProgramId,
                f"{taskDirectory}/{autoProcProgramId}_stderr.log",
                tb + "\n" + str(e),
            )


def check_state():
    from ..mimosa.autoproc import (
        get_processing_jobs,
        get_processing_parameter,
        update_autoproc_program,
    )

    for processingJob in get_processing_jobs(since_days=-5):
        job_id = get_processing_parameter(
            "ewoks_job_id", processingJob.ProcessingJobParameters
        )

        if not job_id:
            logger.warning(
                f"ProcessingJob {processingJob.processingJobId} does not have a `job_id`"
            )
            continue

        try:
            events = get_workflow_events(job_id)
        except RuntimeError as e:
            logger.warning(f"Could not get workflow events for job_id `{job_id}`: {e}")
            events = None

        if not events:
            continue
        jobs = events.get("jobs", [])
        if not jobs:
            continue

        job_events = jobs[0]
        for event in job_events:
            # This is the end of workflow execution event, update the state
            if event["type"] == "end" and event["context"] == "job":
                taskDirectory = get_processing_parameter(
                    "taskDirectory", processingJob.ProcessingJobParameters
                )
                autoProcProgramId = processingJob.AutoProcProgram[0].autoProcProgramId

                # The workflow failed
                if event["error"]:
                    logger.info(f"Workflow `{job_id}` failed")
                    update_autoproc_program(
                        autoProcProgramId,
                        {
                            "processingEndTime": True,
                            "processingStatus": 0,
                            "processingMessage": "processing failed",
                        },
                    )

                    if taskDirectory:
                        attach_and_save_log(
                            autoProcProgramId,
                            f"{taskDirectory}/{autoProcProgramId}_stderr.log",
                            event["error_traceback"],
                        )

                # Workflow succeeded
                else:
                    logger.info(f"Workflow `{job_id}` finished successfully")
                    update_autoproc_program(
                        autoProcProgramId,
                        {
                            "processingEndTime": True,
                            "processingStatus": 1,
                            "processingMessage": "processing success",
                        },
                    )

                if taskDirectory:
                    attach_and_save_log(
                        autoProcProgramId,
                        f"{taskDirectory}/{autoProcProgramId}_events.log",
                        json.dumps(job_events, indent=2),
                    )


def attach_and_save_log(
    autoProcProgramId: int,
    fullFilePath: str,
    contents: str,
    overwrite_log: bool = False,
) -> Optional[int]:
    from ..mimosa.autoproc import add_autoproc_attachment

    try:
        filePath = os.path.dirname(fullFilePath)
        fileName = os.path.basename(fullFilePath)

        if not os.path.exists(filePath):
            os.makedirs(filePath)

        with open(fullFilePath, "w") as fh:
            fh.write(contents)

        return add_autoproc_attachment(
            autoProcProgramId, filePath, fileName, "log", createOrUpdate=overwrite_log
        )
    except Exception:
        logger.exception("Could not save log file")
