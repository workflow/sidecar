import logging
from typing import Any, Optional

from pydantic import BaseModel

logger = logging.getLogger(__name__)


class BaseMessage(BaseModel):
    __task__ = None
    dataCollectionId: int


class WorkflowMessage(BaseMessage):
    __task__ = "execute_graph"
    workflow: str
    parameters: Optional[dict[str, Any]] = {}
    mimosa_register: Optional[bool] = True


class MimasMessage(BaseMessage):
    __task__ = "mimas"
    event: str


MESSAGE_TYPES = [
    WorkflowMessage,
    MimasMessage,
]


def parse_message(headers: dict[str, Any], payload: Any):
    logger.info(f"Parsing message: `{payload}`")
    # Legacy celery message
    if isinstance(payload, list):
        logger.info("Got legacy celery message")
        return WorkflowMessage(
            workflow=payload[0][0],
            dataCollectionId=payload[1]["dataCollectionId"],
            parameters=payload[1]["parameters"],
        )

    if isinstance(payload, dict):
        if "task" in payload:
            task = payload.pop("task")
            for message_type in MESSAGE_TYPES:
                if message_type.__task__ == task:
                    return message_type(**payload)

    logger.warning("Message not successfully parsed")
