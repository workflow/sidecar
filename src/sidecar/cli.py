import argparse

from .kombu.message import send_message
from .server.messages import WorkflowMessage


def execute_graph(args=None) -> None:
    parser = argparse.ArgumentParser(
        usage="sidecar.execute_graph <workflow> <dataCollectionId>"
    )
    parser.add_argument("workflow", type=str)
    parser.add_argument("dataCollectionId", type=int)
    parser.add_argument(
        "-p",
        "--parameters",
        metavar="KEY=VALUE",
        nargs="+",
        help="Set a number of key-value parameters",
    )

    args = parser.parse_args(args)

    parameters = {}
    if args.parameters:
        for pairs in args.parameters:
            kv = pairs.split("=")
            if len(kv) > 1:
                parameters[kv[0]] = kv[1]

    send_message(
        WorkflowMessage(
            workflow=args.workflow,
            dataCollectionId=args.dataCollectionId,
            parameters=parameters,
        )
    )
