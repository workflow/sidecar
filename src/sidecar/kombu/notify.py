import argparse
from typing import Any

from kombu import Connection, Exchange, Queue


def notify(beamline: str, message: dict[str, Any]) -> None:
    from ..settings.broker import settings

    with Connection(settings.broker_url) as conn:
        exchange = Exchange("notification", "direct", durable=True)
        queue = Queue(
            f"notification.{beamline}",
            exchange=exchange,
            durable=False,
            routing_key=f"notification.{beamline}",
        )
        producer = conn.Producer(serializer="json")
        producer.publish(
            message,
            exchange=exchange,
            routing_key=f"notification.{beamline}",
            declare=[queue],
        )


def send_notification(args=None) -> None:
    parser = argparse.ArgumentParser(usage="sidecar.notify <beamline>")
    parser.add_argument("beamline", type=str)
    parser.add_argument(
        "-p",
        "--parameters",
        metavar="KEY=VALUE",
        nargs="+",
        help="Set a number of key-value parameters",
    )

    args = parser.parse_args(args)

    parameters = {}
    if args.parameters:
        for pairs in args.parameters:
            kv = pairs.split("=")
            if len(kv) > 1:
                parameters[kv[0]] = kv[1]

    print(f"Sending to `{args.beamline}`:")
    print(parameters)
    notify(args.beamline, parameters)
