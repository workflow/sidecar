from kombu import Connection, Queue

from ..server.messages import BaseMessage


def send_message(message: BaseMessage):
    from ..settings.broker import settings

    with Connection(settings.broker_url) as conn:
        queue = Queue(settings.broker_queue, durable=True)
        producer = conn.Producer(serializer="json")
        producer.publish(
            {"task": message.__task__, **message.model_dump()},
            routing_key=settings.broker_queue,
            declare=[queue],
        )
