from pydantic_settings import BaseSettings, SettingsConfigDict


class MimosaSettings(BaseSettings):
    mimosa_database_url: str
    mimosa_database_user: str
    mimosa_database_password: str
    mimosa_database_pool: int = 10
    mimosa_database_overflow: int = 20

    model_config = SettingsConfigDict(
        env_file=".env", env_file_encoding="utf-8", extra="ignore"
    )


settings = MimosaSettings()
