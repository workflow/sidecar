import os
from typing import Dict, Any, Union
from pydantic_settings import BaseSettings

try:
    from blissdata.beacon.files import read_config
except ImportError:
    read_config = None


def _get_value(settings: Union[BaseSettings, Dict], var_name: str):
    if isinstance(settings, dict):
        v = settings.get(var_name.lower())
        if v is not None:
            return v
    return os.environ.get(var_name.upper(), None)


def ewoks_config_settings_source(
    settings: Union[BaseSettings, Dict],
) -> Dict[str, Any]:
    """
    Read some settings from the ewoks settings if available.
    """
    var_name = "EWOKS_CONFIG"
    config_url = _get_value(settings, var_name)
    if config_url is None:
        return {}
    if read_config is None:
        raise RuntimeError(
            f"{var_name} is defined but blissdata is not installed in this python env"
        )

    ewoks_config = read_config(config_url)
    celery_config = ewoks_config.get("celery", {})
    result = {}
    if "celery_broker_url" in celery_config:
        result["broker_url"] = celery_config["celery_broker_url"]
    return result


def sidecar_yml_settings_source(settings: BaseSettings) -> Dict[str, Any]:
    """
    Read sidecar settings from a list of resources, including files and beacon paths.
    """
    config_urls = os.environ.get("SIDECAR_CONFIG", None)
    if config_urls is None:
        return {}
    config_urls = config_urls.split(",")
    if read_config is None:
        raise RuntimeError(
            "SIDECAR_CONFIG is defined but blissdata is not installed in this python env"
        )

    result = {}
    for url in config_urls:
        config = read_config(url)
        result.update(config)

    result.update(ewoks_config_settings_source(result))

    return result
