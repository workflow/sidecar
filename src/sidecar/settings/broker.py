from typing import Optional

from pydantic_settings import BaseSettings, SettingsConfigDict


class BrokerSettings(BaseSettings):
    broker_url: str
    """
    Url to messaging broker
    """
    broker_queue: Optional[str] = "default"

    model_config = SettingsConfigDict(
        env_file=".env", env_file_encoding="utf-8", extra="ignore"
    )


settings = BrokerSettings()
