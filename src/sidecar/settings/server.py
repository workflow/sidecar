from typing import Optional, Tuple, Type

from pydantic_settings import (
    BaseSettings,
    PydanticBaseSettingsSource,
    SettingsConfigDict,
)
from .esrf_settings import sidecar_yml_settings_source
from .mimosa import MimosaSettings
from .broker import BrokerSettings


class ServerSettings(MimosaSettings, BrokerSettings):
    ewoks_server_url: str
    """
    URL to ewoksserver
    """
    ewoks_config: Optional[str] = None

    graylog_url: Optional[str] = None

    mimas_specifications: Optional[str]

    system_test_config: Optional[str] = None

    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> Tuple[PydanticBaseSettingsSource, ...]:
        return (
            lambda: sidecar_yml_settings_source(settings_cls),
            env_settings,
            dotenv_settings,
            init_settings,
            file_secret_settings,
        )


settings = ServerSettings()
