import argparse

from ..mimosa import injector
from .base import execute
from .classes import MimasEvent


def main(args=None) -> None:
    parser = argparse.ArgumentParser(usage="sidecar.mimas <dataCollectionId>")
    parser.add_argument("dataCollectionId", type=int)
    parser.add_argument(
        "-x", "--execute", action="store_true", help="Execute the mimas task"
    )
    parser.add_argument(
        "-e",
        "--event",
        type=str,
        choices=["start", "end"],
        default="start",
        help="Event to execute",
    )

    args = parser.parse_args(args)

    if args.execute:
        from ..kombu.message import send_message
        from ..server.messages import MimasMessage

        send_message(
            MimasMessage(event=args.event, dataCollectionId=args.dataCollectionId)
        )

    else:
        metadata = injector.metadata(args.dataCollectionId)

        print(
            f"Mimas for dataCollectionId `{args.dataCollectionId}` in session `{metadata['session']}` will invoke"
        )
        for event in (MimasEvent.START, MimasEvent.END):
            print(f"  At {event.name}:")
            invocations = execute(event.name, args.dataCollectionId)
            for invocation in invocations:
                print(f"    {invocation.graph}")

            if not invocations:
                print("    Nothing")
