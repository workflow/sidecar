from ..specification import (
    match_specification,
    EventSpecification,
    ExperimentTypeSpecification,
)
from ..classes import (
    MimasScenario,
    MimasGraphInvocation,
    MimasEvent,
    MimasExperimentType,
)

is_start = EventSpecification(MimasEvent.START)
is_end = EventSpecification(MimasEvent.END)

is_xrf_map = ExperimentTypeSpecification(MimasExperimentType.XRF_MAP)


@match_specification(is_end & is_xrf_map)
def handle_xrf_map_end(scenario: MimasScenario) -> list[MimasGraphInvocation]:
    return [
        MimasGraphInvocation(dataCollectionId=scenario.dataCollectionId, graph="pymca")
    ]
