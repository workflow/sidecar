import importlib
import pkgutil
from typing import Any

from celery.utils.log import get_task_logger

from ..mimosa import injector
from ..settings.server import settings

from .classes import (
    MimasEvent,
    MimasGraphInvocation,
    MimasScenario,
    MimasExperimentType,
)

logger = get_task_logger(__name__)


def execute(event: str, dataCollectionId: int) -> list[MimasGraphInvocation]:
    metadata = injector.metadata(dataCollectionId)
    scenario = retrieve_scenario(event, dataCollectionId, metadata)
    return handle_scenario(scenario)


def retrieve_scenario(
    event: str, dataCollectionId: int, metadata: dict[str, Any]
) -> MimasScenario:
    experimentType_safe = metadata["experimentType"].replace(" ", "_")
    return MimasScenario(
        dataCollectionId=dataCollectionId,
        eventType=MimasEvent[event.upper()],
        experimentType=MimasExperimentType[experimentType_safe.upper()],
        beamlineName=metadata["beamLineName"],
        runStatus=metadata["runStatus"],
        session=metadata["session"],
        proposal=metadata["proposal"],
    )


def handle_scenario(scenario: MimasScenario) -> list[MimasGraphInvocation]:
    tasks: list[MimasGraphInvocation] = []

    implementors = settings.mimas_specifications
    if not implementors:
        raise RuntimeError("No specifications module defined")

    mod = importlib.import_module(implementors)
    for loader, modname, _ in pkgutil.walk_packages(
        path=mod.__path__, prefix=mod.__name__ + ".", onerror=lambda x: None
    ):
        sub_mod = loader.find_module(modname).load_module(modname)
        for symbol in sub_mod.__dict__.values():
            if callable(symbol) and hasattr(symbol, "is_specification"):
                tasks.extend(symbol(scenario))

    return tasks
