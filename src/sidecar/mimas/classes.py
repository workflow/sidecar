import enum
from typing import Optional
from pydantic import BaseModel


MimasEvent = enum.Enum("MimasEvent", "START END")

MimasExperimentType = enum.Enum(
    "MimasExperimentType",
    "OSC MESH SAD ENERGY_SCAN XRF_MAP XRF_MAP_XAS XRF_SPECTRUM EXPERIMENT TOMO UNDEFINED",
)


class MimasModel(BaseModel):
    class Config:
        frozen = True


class MimasMimosaUnitCell(MimasModel):
    a: float
    b: float
    c: float
    alpha: float
    beta: float
    gamma: float

    @property
    def string(self) -> str:
        return f"{self.a},{self.b},{self.c},{self.alpha},{self.beta},{self.gamma}"


class MimasMimosaSpaceGroup(MimasModel):
    symbol: str

    @property
    def string(self) -> str:
        import gemmi

        return gemmi.SpaceGroup(self.symbol).hm.replace(" ", "")


class MimasScenario(MimasModel):
    dataCollectionId: int
    experimentType: MimasExperimentType
    eventType: MimasEvent
    beamlineName: str
    runStatus: Optional[str]
    session: str
    proposal: str


class MimasGraphInvocation(MimasModel):
    dataCollectionId: int
    graph: str
