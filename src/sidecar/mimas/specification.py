from __future__ import annotations
import functools
from abc import abstractmethod
from dataclasses import dataclass
from typing import Any, Optional, Callable, TypeVar
import logging

from .classes import (
    MimasEvent,
    MimasExperimentType,
    MimasScenario,
    MimasGraphInvocation,
)

logger = logging.getLogger(__name__)


class BaseSpecification:
    @abstractmethod
    def is_satisfied_by(self, candidate: Any) -> bool:
        raise NotImplementedError()

    def __and__(self, other: BaseSpecification) -> AndSpecification:
        return AndSpecification(self, other)

    def __or__(self, other: BaseSpecification) -> OrSpecification:
        return OrSpecification(self, other)

    def __invert__(self) -> InvertSpecification:
        return InvertSpecification(self)


@dataclass(frozen=True)
class AndSpecification(BaseSpecification):
    first: BaseSpecification
    second: BaseSpecification

    def is_satisfied_by(self, candidate: Any) -> bool:
        return self.first.is_satisfied_by(candidate) and self.second.is_satisfied_by(
            candidate
        )


@dataclass(frozen=True)
class OrSpecification(BaseSpecification):
    first: BaseSpecification
    second: BaseSpecification

    def is_satisfied_by(self, candidate: Any) -> bool:
        return self.first.is_satisfied_by(candidate) or self.second.is_satisfied_by(
            candidate
        )


@dataclass(frozen=True)
class InvertSpecification(BaseSpecification):
    subject: BaseSpecification

    def is_satisfied_by(self, candidate: Any) -> bool:
        return not self.subject.is_satisfied_by(candidate)


class ScenarioSpecification(BaseSpecification):
    @abstractmethod
    def is_satisfied_by(self, candidate: MimasScenario) -> bool:
        raise NotImplementedError()


@dataclass(frozen=True)
class BeamlineSpecification(ScenarioSpecification):
    beamlineName: Optional[str] = None
    beamlineNames: Optional[set[str]] = None

    def is_satisfied_by(self, candidate: MimasScenario) -> bool:
        return (
            (self.beamlineName and candidate.beamlineName == self.beamlineName)
            or (self.beamlineNames and candidate.beamlineName in self.beamlineNames)
            or False
        )


@dataclass(frozen=True)
class EventSpecification(ScenarioSpecification):
    eventType: MimasEvent

    def is_satisfied_by(self, candidate: MimasScenario) -> bool:
        return candidate.eventType == self.eventType


@dataclass(frozen=True)
class ExperimentTypeSpecification(ScenarioSpecification):
    experimentType: MimasExperimentType

    def is_satisfied_by(self, candidate: MimasScenario) -> bool:
        return candidate.experimentType == self.experimentType


F = TypeVar("F", bound=Callable[..., Any])


def match_specification(specification: BaseSpecification) -> F:
    def outer_wrapper(handler: F) -> F:
        @functools.wraps(handler)
        def inner_wrapper(scenario: MimasScenario) -> list[MimasGraphInvocation]:
            if specification.is_satisfied_by(scenario):
                return handler(scenario)
            return []

        inner_wrapper.is_specification = True
        return inner_wrapper

    return outer_wrapper
