import queue as _queue
import time
from typing import Any

from kombu import Connection, Queue
from sidecar.settings.broker import settings


def consume(
    beamline: str,
    test_message: dict[str, Any],
    timeout: int = 5,
) -> None:
    with Connection(settings.broker_url) as conn:
        queue = Queue(
            f"notification.{beamline}",
            exchange="notification",
            durable=False,
        )
        start = time.time()
        while True:
            with conn.SimpleBuffer(queue) as simple_queue:
                try:
                    message = simple_queue.get(block=True, timeout=1)
                    print("message", message)
                    if message.payload.get("uuid") == test_message["uuid"]:
                        assert message.payload == test_message  # nosec
                        message.ack()
                        break
                except _queue.Empty:
                    pass

            if time.time() - start > timeout:
                raise RuntimeError(f"Timed out after {timeout}s waiting for message")
