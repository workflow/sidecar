-- Create the sidecar processing role.
CREATE ROLE IF NOT EXISTS sidecar;

-- You must also create a database user and grant this role to them, e.g.
-- CREATE USER sidecar_central@'%' IDENTIFIED BY 'the_sidecar_password';
-- GRANT sidecar to sidecar_central@'%';
-- SET DEFAULT ROLE sidecar FOR sidecar_central@'%';

-- Grants for sidecar

GRANT SELECT ON DataCollection TO 'sidecar';
GRANT SELECT ON Detector TO 'sidecar';
GRANT SELECT ON DataCollectionGroup TO 'sidecar';
GRANT SELECT ON BLSession TO 'sidecar';
GRANT SELECT ON Proposal TO 'sidecar';
GRANT SELECT ON Protein TO 'sidecar';
GRANT SELECT ON Crystal TO 'sidecar';
GRANT SELECT ON BLSample TO 'sidecar';
GRANT SELECT ON GridInfo TO 'sidecar';
GRANT SELECT ON DiffractionPlan TO 'sidecar';

GRANT SELECT, UPDATE, INSERT ON ProcessingJob TO 'sidecar';
GRANT SELECT, UPDATE, INSERT ON ProcessingJobParameter TO 'sidecar';
GRANT SELECT, UPDATE, INSERT ON AutoProcProgram TO 'sidecar';
GRANT SELECT, UPDATE, INSERT ON AutoProcProgramAttachment TO 'sidecar';
GRANT SELECT, UPDATE, INSERT ON AutoProcProgramMessage TO 'sidecar';
GRANT SELECT, UPDATE, INSERT ON XRFFluorescenceMapping TO 'sidecar';
GRANT SELECT, UPDATE, INSERT ON XRFFluorescenceMappingROI TO 'sidecar';
