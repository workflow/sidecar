from sidecar.mimas.base import handle_scenario
from sidecar.mimas.classes import MimasEvent, MimasExperimentType, MimasScenario


def test_mimas_no_graphs() -> None:
    scenario = MimasScenario(
        dataCollectionId=1,
        eventType=MimasEvent.START,
        experimentType=MimasExperimentType.XRF_MAP,
        beamlineName="id00",
        runStatus="running",
        session="blc00001-1",
        proposal="blc00001",
    )

    graphs = handle_scenario(scenario)
    assert not graphs


def test_mimas_graphs() -> None:
    scenario = MimasScenario(
        dataCollectionId=1,
        eventType=MimasEvent.END,
        experimentType=MimasExperimentType.XRF_MAP,
        beamlineName="id00",
        runStatus="finished",
        session="blc00001-1",
        proposal="blc00001",
    )

    graphs = handle_scenario(scenario)
    assert len(graphs) == 1
