import pydantic
import pytest
from sidecar.mimas.classes import MimasExperimentType, MimasScenario, MimasEvent


def test_mimas_invalid_event_type() -> None:
    with pytest.raises(pydantic.ValidationError):
        MimasScenario(
            dataCollectionId=1,
            eventType="moo",
            experimentType=MimasExperimentType.XRF_MAP,
            beamlineName="id00",
            runStatus="finished",
            session="blc00001-1",
            proposal="blc00001",
        )


def test_mimas_invalid_exp_type() -> None:
    with pytest.raises(pydantic.ValidationError):
        MimasScenario(
            dataCollectionId=1,
            eventType=MimasEvent.START,
            experimentType="moo",
            beamlineName="id00",
            runStatus="finished",
            session="blc00001-1",
            proposal="blc00001",
        )
