from sidecar.mimosa.autoproc import get_processing_jobs, get_processing_parameter


def test_get_processing_jobs():
    jobs = get_processing_jobs()
    print(jobs)
    if jobs:
        job_id = get_processing_parameter("job_id", jobs[0].ProcessingJobParameters)
        print(job_id)
