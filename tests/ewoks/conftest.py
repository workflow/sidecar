import os
import pytest


@pytest.fixture
def test_workflows() -> str:
    return os.path.join(os.path.dirname(__file__), "resources", "workflows")
