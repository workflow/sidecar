import os
import uuid

from ewoks import execute_graph
from sidecar.test_utils import consume


def test_notify_task(test_workflows: str) -> None:
    beamline = "bl"
    message = {"test": 123, "uuid": str(uuid.uuid4())}
    execute_graph(
        os.path.join(test_workflows, "test-notify.json"),
        inputs=[
            {"name": "beamline", "value": beamline},
            {"name": "message", "value": message},
        ],
    )

    consume(beamline, message)
