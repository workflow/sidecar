from ewoks import execute_graph
import os
import pytest


def test_filewatcher_bliss(test_workflows: str, test_data: str) -> None:
    execute_graph(
        os.path.join(test_workflows, "test-filewatcher.json"),
        inputs=[
            {"name": "file_path", "value": os.path.join(test_data, "bliss", "scan.h5")},
            {"name": "hdf5_path", "value": "1.1/measurement"},
            {"name": "expected_points", "value": 26},
            {"name": "timeout", "value": 1},
        ],
    )


def test_filewatcher_bliss_timeout(test_workflows: str, test_data: str) -> None:
    with pytest.raises(RuntimeError) as exception_info:
        execute_graph(
            os.path.join(test_workflows, "test-filewatcher.json"),
            inputs=[
                {
                    "name": "file_path",
                    "value": os.path.join(test_data, "bliss", "scan.h5"),
                },
                {"name": "hdf5_path", "value": "1.1/measurement"},
                {"name": "expected_points", "value": 27},
                {"name": "timeout", "value": 2},
            ],
        )

    exception = exception_info.value
    assert "Timeout" in str(exception.__context__)


def test_filewatcher_lima(test_workflows: str, test_data: str) -> None:
    execute_graph(
        os.path.join(test_workflows, "test-filewatcher-lima.json"),
        inputs=[
            {
                "name": "image_path",
                "value": os.path.join(test_data, "lima", "scan0001", "lima_simulator"),
            },
            {"name": "expected_images", "value": 6},
            {"name": "timeout", "value": 1},
        ],
    )


def test_filewatcher_lima_timeout(test_workflows: str, test_data: str) -> None:
    with pytest.raises(RuntimeError) as exception_info:
        execute_graph(
            os.path.join(test_workflows, "test-filewatcher-lima.json"),
            inputs=[
                {
                    "name": "image_path",
                    "value": os.path.join(
                        test_data, "lima", "scan0001", "lima_simulator"
                    ),
                },
                {"name": "expected_images", "value": 7},
                {"name": "timeout", "value": 1},
            ],
        )
    exception = exception_info.value
    assert "Timeout" in str(exception.__context__)
