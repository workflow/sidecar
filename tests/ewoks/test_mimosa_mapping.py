import os

from ewoks import execute_graph

from sidecar.mimosa.mapping import get_maps, get_mapping_rois
from sidecar.utils import gzip_json


def test_mapping_add_map(test_workflows: str) -> None:
    dataCollectionId = 1
    xrfFluorescenceMappingROIId = 1
    data = [1, 2, 3, 4, 5]

    result = execute_graph(
        os.path.join(test_workflows, "test-mapping-map.json"),
        inputs=[
            {"name": "dataCollectionId", "value": dataCollectionId},
            {
                "name": "xrfFluorescenceMappingROIId",
                "value": xrfFluorescenceMappingROIId,
            },
            {"name": "data", "value": data},
        ],
    )

    maps = get_maps(dataCollectionId)
    for map_ in maps:
        if map_.xrfFluorescenceMappingId == result["xrfFluorescenceMappingId"]:
            break
    else:
        raise RuntimeError("Newly created map not found")

    assert map_.xrfFluorescenceMappingROIId == xrfFluorescenceMappingROIId
    assert map_.data == gzip_json(data)


def test_mapping_add_roi(test_workflows: str) -> None:
    startEnergy = 5
    endEnergy = 10
    element = "Fe"
    edge = "K"
    blSampleId = 1

    result = execute_graph(
        os.path.join(test_workflows, "test-mapping-roi.json"),
        inputs=[
            {"name": "startEnergy", "value": startEnergy},
            {
                "name": "endEnergy",
                "value": endEnergy,
            },
            {"name": "element", "value": element},
            {"name": "edge", "value": edge},
            {"name": "blSampleId", "value": blSampleId},
        ],
    )

    rois = get_mapping_rois(blSampleId)
    for roi in rois:
        if roi.xrfFluorescenceMappingROIId == result["xrfFluorescenceMappingROIId"]:
            break
    else:
        raise RuntimeError("Newly created map ROI not found")

    assert roi.startEnergy == startEnergy
    assert roi.endEnergy == endEnergy
    assert roi.element == element
    assert roi.edge == edge
