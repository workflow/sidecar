import os
from typing import Optional

from ewoks import execute_graph

from sidecar.mimosa.session import get_session
from sidecar.mimosa import models


def test_autoproc_add_attachment(test_workflows: str) -> None:
    autoProcProgramId = 1
    filePath = "/data/path/test"
    fileName = "test.h5"

    result = execute_graph(
        os.path.join(test_workflows, "test-autoproc-attachment.json"),
        inputs=[
            {"name": "autoProcProgramId", "value": autoProcProgramId},
            {"name": "nxdata_url", "value": f"{filePath}/{fileName}::/path"},
        ],
    )

    with get_session() as ses:
        autoProcProgramAttachment: Optional[models.AutoProcProgramAttachment] = (
            ses.query(models.AutoProcProgramAttachment)
            .filter(
                models.AutoProcProgramAttachment.autoProcProgramAttachmentId
                == result["autoProcProgramAttachmentId"]
            )
            .first()
        )

        if not autoProcProgramAttachment:
            raise RuntimeError(
                f"Couldnt find autoProcProgramAttachmentId {result['autoProcProgramAttachmentId']}"
            )

        assert autoProcProgramAttachment.fileName == fileName
        assert autoProcProgramAttachment.filePath == filePath


def test_autoproc_add_message(test_workflows: str) -> None:
    autoProcProgramId = 1
    severity = "ERROR"
    message = "test message"
    description = "test description"

    result = execute_graph(
        os.path.join(test_workflows, "test-autoproc-message.json"),
        inputs=[
            {"name": "autoProcProgramId", "value": autoProcProgramId},
            {"name": "severity", "value": severity},
            {"name": "message", "value": message},
            {"name": "description", "value": description},
        ],
    )

    with get_session() as ses:
        autoProcProgramMessage: Optional[models.AutoProcProgramMessage] = (
            ses.query(models.AutoProcProgramMessage)
            .filter(
                models.AutoProcProgramMessage.autoProcProgramMessageId
                == result["autoProcProgramMessageId"]
            )
            .first()
        )

        if not autoProcProgramMessage:
            raise RuntimeError(
                f"Couldnt find autoProcProgramMessageId {result['autoProcProgramMessageId']}"
            )

        assert autoProcProgramMessage.severity == severity
        assert autoProcProgramMessage.message == message
        assert autoProcProgramMessage.description == description
