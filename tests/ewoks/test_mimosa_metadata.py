from sidecar.mimosa.injector import get_paths, metadata


def test_mimosa_metadata() -> None:
    datacollection = metadata(1)
    assert datacollection["session"] == "blc00001-1"


def test_mimosa_metadata_data_policy() -> None:
    imageDirectory = "/data/visitor/blc00001/id00/20200101/RAW_DATA/sample/dataset"
    session, working = get_paths(imageDirectory)
    assert session == "/data/visitor/blc00001/id00/20200101"
    assert (
        working == "/data/visitor/blc00001/id00/20200101/PROCESSED_DATA/sample/dataset"
    )


def test_mimosa_metadata_data_policy_v2() -> None:
    imageDirectory = "/data/visitor/blc00001/id00/20200101/raw/sample/dataset"
    _, working = get_paths(imageDirectory)
    # TODO: Session wrongly resolved for v2
    # assert session == "/data/visitor/blc00001/id00/20200101"
    assert working == "/data/visitor/blc00001/id00/20200101/processed/sample/dataset"


def test_mimosa_metadata_data_policy_v1() -> None:
    imageDirectory = "/data/visitor/blc00001/id00/20200101/sample/dataset"
    _, working = get_paths(imageDirectory)
    # TODO: Session wrongly resolved for v1
    # assert session == "/data/visitor/blc00001/id00/20200101"
    assert working == "/data/visitor/blc00001/id00/20200101/processed/sample/dataset"
