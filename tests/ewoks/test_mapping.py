import os

from ewoks import execute_graph

from sidecar.mimosa.mapping import get_maps, add_mapping_roi


def test_mapping_scalar(test_workflows: str, test_data: str) -> None:
    file = os.path.join(test_data, "bliss", "scan_spectra.h5")
    dataCollectionId = 1
    autoProcProgramId = 1
    metadata = {
        "gridInfoId": 1,
        "fileTemplate": os.path.basename(file),
        "imageDirectory": os.path.dirname(file),
        "imageContainerSubPath": "1.1/measurement",
        "steps_x": 6,
        "steps_y": 6,
    }
    scalar_name = "diode"

    result = execute_graph(
        os.path.join(test_workflows, "test-scalar-map.json"),
        inputs=[
            {"name": "dataCollectionId", "value": dataCollectionId},
            {"name": "autoProcProgramId", "value": autoProcProgramId},
            {"name": "metadata", "value": metadata},
            {"name": "scalar_name", "value": scalar_name},
        ],
    )

    maps = get_maps(dataCollectionId)
    for map_ in maps:
        if map_.xrfFluorescenceMappingId == result["map_ids"][0]:
            break
    else:
        raise RuntimeError("Newly created map not found")


def test_mapping_from_roi(test_workflows: str, test_data: str) -> None:
    file = os.path.join(test_data, "bliss", "scan_spectra.h5")
    xrfFluorescenceMappingROIId = add_mapping_roi(12000, 12500, "Se", "K", blSampleId=1)
    dataCollectionId = 1
    autoProcProgramId = 1
    metadata = {
        "gridInfoId": 1,
        "fileTemplate": os.path.basename(file),
        "imageDirectory": os.path.dirname(file),
        "imageContainerSubPath": "1.1/measurement",
        "blSampleId": 1,
        "steps_x": 6,
        "steps_y": 6,
    }

    result = execute_graph(
        os.path.join(test_workflows, "test-roi-map.json"),
        inputs=[
            {"name": "dataCollectionId", "value": dataCollectionId},
            {"name": "autoProcProgramId", "value": autoProcProgramId},
            {"name": "metadata", "value": metadata},
        ],
    )

    if not result["map_ids"]:
        raise RuntimeError("No maps created")

    maps = get_maps(dataCollectionId)
    for map_ in maps:
        if (
            map_.xrfFluorescenceMappingROIId == xrfFluorescenceMappingROIId
            and map_.xrfFluorescenceMappingId == result["map_ids"][0]
        ):
            break
    else:
        raise RuntimeError("Newly created map not found")
