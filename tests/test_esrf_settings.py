import pytest
import os


SIDECAR_CONFIG = """
broker_queue: 'default'
mimas_specifications: 'sidecar.mimas.specifications'
"""

EWOKS_CONFIG = """
celery:
    celery_broker_url: "redis://localhost:10002/2"
    result_backend: "redis://localhost:10002/3"
    result_serializer: "pickle"
    accept_content: ["application/json", "application/x-python-serialize"]
    result_expires: 600
"""

ESRF_DATA_POLICY_V1_CONFIG = """
scan_saving:
    class: ESRFScanSaving
    beamline: id00
    tmp_data_root: /tmp/scans/tmp
    visitor_data_root: /tmp/scans/visitor
    inhouse_data_root: /tmp/scans/inhouse
    icat_tmp_data_root: /tmp/scans/fsi/{beamline}/tmp_icat
"""

PRIVATE_CONFIG = """
mimosa_database_url: 'localhost:4406/test'
mimosa_database_user: 'foo'
mimosa_database_password: 'bar'
"""


@pytest.fixture
def esrf_config_env(tmp_path):
    private_path = tmp_path / "private.yml"
    ewoks_path = tmp_path / "ewoks.yml"
    data_policy_path = tmp_path / "data_policy.yml"
    sidecar_path = tmp_path / "sidecar.yml"

    with open(private_path, "wt") as f:
        f.write(PRIVATE_CONFIG)
    with open(ewoks_path, "wt") as f:
        f.write(EWOKS_CONFIG)
    with open(data_policy_path, "wt") as f:
        f.write(ESRF_DATA_POLICY_V1_CONFIG)
    with open(sidecar_path, "wt") as f:
        f.write(SIDECAR_CONFIG)
        f.write(f"ewoks_config: '{ewoks_path}'\n")
        f.write(f"data_policy_config: '{data_policy_path}'\n")

    prev_beacon = os.environ.get("BEACON_HOST")
    prev_sidecar = os.environ.get("SIDECAR_CONFIG")
    os.environ["BEACON_HOST"] = "localhost:10001"
    os.environ["SIDECAR_CONFIG"] = f"{sidecar_path},{private_path}"
    try:
        yield
    finally:
        if prev_beacon is None:
            del os.environ["BEACON_HOST"]
        else:
            os.environ["BEACON_HOST"] = prev_beacon
        if prev_beacon is None:
            del os.environ["SIDECAR_CONFIG"]
        else:
            os.environ["SIDECAR_CONFIG"] = prev_sidecar


def test_settings_v1(esrf_config_env):
    from sidecar.settings.server import ServerSettings

    s = ServerSettings()
    assert s.mimosa_database_user == "foo"
    assert s.broker_url == "redis://localhost:10002/2"
