from sidecar.system_test.base import SystemTest


def test_system_test(ewoksserver, sidecarserver):
    system_test = SystemTest(
        {
            "Mimas": {
                "start_datacollection": 1,
                "end_datacollection": 1,
                "start_invocations": [],
                "end_invocations": [],
            }
        }
    )
    system_test.run(output_xml="output.xml", raise_on_error=True)
