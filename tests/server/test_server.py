import uuid

from sidecar.kombu.message import send_message
from sidecar.server.messages import WorkflowMessage
from sidecar.test_utils import consume


def test_server(ewoksserver, sidecarserver):
    beamline = "bl"
    message = {"test": 123, "uuid": str(uuid.uuid4())}
    send_message(
        WorkflowMessage(
            workflow="test-notify",
            dataCollectionId=1,
            parameters={"message": message, "beamline": beamline},
        )
    )

    consume(beamline, message)
