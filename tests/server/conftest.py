import os
import pytest
import psutil
import requests
import time
import contextlib
import subprocess
from typing import Generator


@pytest.fixture
def sidecarserver():
    with start_sidecarserver():
        yield


@contextlib.contextmanager
def start_sidecarserver(*cmdline_args: str) -> Generator[None, None, None]:
    """Start sidecar.server"""
    p = subprocess.Popen(["sidecar.server", *cmdline_args])
    try:
        time.sleep(5)
        yield
    finally:
        wait_terminate(p, check_children=True)


@pytest.fixture
def ewoksserver():
    with start_ewoksserver(
        "--config",
        os.path.join(os.path.abspath(os.path.dirname(__file__)), "config.py"),
    ):
        yield


@contextlib.contextmanager
def start_ewoksserver(*cmdline_args: str) -> Generator[None, None, None]:
    """Start ewoksserver"""
    p = subprocess.Popen(["ewoks-server", *cmdline_args])
    try:
        wait_ewoksserver()
        yield
    finally:
        wait_terminate(p, check_children=True)


def wait_ewoksserver() -> bool:
    for _ in range(10):
        try:
            resp = requests.get("http://localhost:8000/api/workflows")
            if resp.status_code == 200:
                return True
        except requests.exceptions.ConnectionError:
            pass
        time.sleep(1)

    raise RuntimeError("ewoksserver did not start up")


def wait_terminate(
    process: subprocess.Popen[bytes], timeout: int = 10, check_children: bool = False
) -> None:
    """
    Try to terminate a process then kill it.

    This ensure the process is terminated.

    Arguments:
        process: A process object from `subprocess` or `psutil`, or an PID int
        timeout: Timeout to way before using a kill signal
        check_children: If true, check children pid and force there termination
    Raises:
        gevent.Timeout: If the kill fails
    """
    children = []
    if isinstance(process, int):
        try:
            name = str(process)
            process = psutil.Process(process)
        except Exception:
            # PID is already dead
            return
    else:
        name = repr(" ".join(process.args))
        if process.poll() is not None:
            print(f"Process {name} already terminated with code {process.returncode}")
            return

    if check_children:
        if not isinstance(process, psutil.Process):
            process = psutil.Process(process.pid)
        children = process.children(recursive=True)

    process.terminate()
    process.wait()

    if check_children:
        for i in range(10):
            _done, alive = psutil.wait_procs(children, timeout=1)
            if not alive:
                break
            for p in alive:
                try:
                    if i < 3:
                        p.terminate()
                    else:
                        p.kill()
                except psutil.NoSuchProcess:
                    pass
        else:
            raise RuntimeError(
                "Timeout expired after 10 seconds. Process %s still alive." % alive
            )
