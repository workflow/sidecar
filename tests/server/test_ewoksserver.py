from sidecar.server.ewoksserver import start_workflow, get_workflow_events


def test_ewoksserver_start_status(ewoksserver):
    job_id = start_workflow("demo")
    assert job_id

    events = get_workflow_events(job_id)
    assert events
