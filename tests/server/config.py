import os

_SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__))

RESOURCE_DIRECTORY = os.path.join(_SCRIPT_DIR, "..", "ewoks", "resources")

EWOKS = {
    "handlers": [
        {
            "class": "ewokscore.events.handlers.Sqlite3EwoksEventHandler",
            "arguments": [
                {
                    "name": "uri",
                    "value": "file:" + os.path.join(_SCRIPT_DIR, "ewoks_events.db"),
                }
            ],
        }
    ]
}
