The system test tool allows the infrastructure related to sidecar to be tested at runtime in a production environment.

It currently provides tests for the following:

- Kombu (loopback)
- Ewoksserver (check ewoksserver is running and can execute the demo workflow)
- Server (check a test graph can be executed from the server, full end-to-end test)
- Mimas (check mimas resolves the correct invocations for a data collection)

The system test can be launched via the cli tool: `sidecar.system_test`

A configuration file must be suppled, an example is provided in `examples/system_test.yml`, and this should be set via the environment variable `SYSTEM_TEST_CONFIG`

The tool returns a non-zero code on failure so can be used in cron jobs or jenkins. It also provides a [junit](https://junit.org/junit5/) test suite output, by default: `output.xml`. Tools such as jenkins can read this and provide stability graphs.
