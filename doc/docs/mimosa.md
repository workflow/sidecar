Sidecar provides [Mimosa](https://gitlab.esrf.fr/ui/mimosa/mimosa) helpers to register entries into `ProcessingJob` and `AutoProcProgram` when a task is executed, and monitor its state. It can also save associated output and result files, as well as store processing messages.

Add an attachment:

```python
from sidecar.mimosa.autoproc import add_autoproc_attachment

autoProcProgramAttachmentId = add_autoproc_attachment(
    autoProcProgramId,
    filePath,
    fileName,
    fileType,
    importanceRank,
)
```

Add a processing message:

```python
from sidecar.mimosa.autoproc import add_autoproc_message

autoProcProgramMessageId = add_autoproc_message(
    autoProcProgramId,
    severity,
    message,
    description
)
```

It also provides a metadata parser, which given a `dataCollectionId` can retireve the associated metadata from Mimosa, this can be useful during data processing:

```python
from sidecar.mimosa import metadata as get_metadata

metadata = get_metadata(dataCollectionId)
print(metadata)

{
  "fileTemplate": "sdf__roi6_4.h5",
  "imageDirectory": "/data/bl/tmp/blc00001/sdf/sdf__roi6_4",
  "imageContainerSubPath": None,
  "numberOfImages": None,
  "exposureTime": None,
  "runStatus": "Failed",
  "experimentType": "XRF map",
  "beamLineName": "bl",
  "session": "blc00001-1",
  "proposal": "blc00001",
  "sequence": None,
  "steps_x": None,
  "steps_y": None,
  "workingDirectory": "/data/bl/tmp/blc00001/sdf/sdf__roi6_4"
}
```
