Sidecar can be used to send notification messages to beamlines during an executing task. The daiquiri server subscribes to its own notification queue and sidecar provides a helper to send a notification to these queues.

```python
from sidecar.kombu.notify import notify

notify("bl", {"key": 1})
```
