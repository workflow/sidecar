pyFAI related ewoks tasks

## Integrate1dMap

Take the result from a `ewoksxrpd.tasks.integrate.IntegrateBlissScan` task and create an `xrfFluorescenceMappingId` with an associated roi. The associated `dataCollectionId` must have a `gridInfo`.

```json
{
    "id": "sidecar.ewoks.pyFAI.integrate.Integrate1dMap0",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.pyFAI.integrate.Integrate1dMap",
},
```

### Inputs

| Parameter         | Type |                                                                              |
| ----------------- | ---- | ---------------------------------------------------------------------------- |
| autoProcProgramId | str  | From `StartJob`                                                              |
| nxdata_url        | str  | nxdata_url to the result from `ewoksxrpd.tasks.integrate.IntegrateBlissScan` |

### Outputs

| Parameter                | Type |                      |
| ------------------------ | ---- | -------------------- |
| xrfFluorescenceMappingId | int  | The resulting map id |
