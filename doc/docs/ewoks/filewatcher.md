## Filewatcher

The filewatcher task can monitor [bliss](https://gitlab.esrf.fr/bliss/bliss) and [lima](https://gitlab.esrf.fr/limagroup/lima) scan files and start sequential tasks based on the number of points currently available.

### Parse Metadata

Parse the Mimosa metadata into something the filewatcher can understand

```json
{
    "id": "parse_input",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.filewatcher.parser.ParseMetadata"
},
```

#### Inputs

| Parameter | Type |                     |
| --------- | ---- | ------------------- |
| metadata  | dict | the Mimosa metadata |

#### Outputs

| Parameter                | Type  |                                         |
| ------------------------ | ----- | --------------------------------------- |
| file_path                | str   | the file path                           |
| hdf5_path                | str   | the path to the data within the h5 file |
| expected_points          | int   | number of points to expect              |
| expected_per_point_delay | float | expected time per point                 |

### Bliss Filewatcher

Watches a bliss scan file for its current number of points. If the file contains a `sequence_of_scans` it will watch the latest scan number.

```json
{
    "id": "wait_for_files",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.filewatcher.bliss.BlissFileWatcher"
},
```

#### Inputs

| Parameter                | Type  |                                                                                                       |
| ------------------------ | ----- | ----------------------------------------------------------------------------------------------------- |
| file_path                | str   | the file path                                                                                         |
| hdf5_path                | str   | the path to the data within the h5 file                                                               |
| expected_points          | int   | number of points to expect                                                                            |
| expected_per_point_delay | float | _optional_: Per point delay, used to calculate timeout                                                |
| timeout                  | int   | _optional_: Timeout in seconds, default 200, only used if `expected_per_point_delay` is not specified |

#### Outputs

| Parameter | Type |                                 |
| --------- | ---- | ------------------------------- |
| running   | bool | if the process is still running |
| finished  | bool | all points found                |

### Lima Filewatcher

Watches a diretory for a series of lima image files and calculates the total number of images available. Either individual images, or batches h5 files.

```json
{
    "id": "wait_for_files",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.filewatcher.lima.LimaFileWatcher"
},
```

#### Inputs

| Parameter                | Type  |                                                                                                       |
| ------------------------ | ----- | ----------------------------------------------------------------------------------------------------- |
| image_path               | str   | directory containing images, will be globbed                                                          |
| expected_images          | int   | number of images to expect                                                                            |
| expected_per_image_delay | float | _optional_: Per image delay, used to calculate timeout                                                |
| timeout                  | int   | _optional_: Timeout in seconds, default 200, only used if `expected_per_image_delay` is not specified |

#### Outputs

| Parameter | Type |                                 |
| --------- | ---- | ------------------------------- |
| running   | bool | if the process is still running |
| finished  | bool | all points found                |
