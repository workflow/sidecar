Sidecar provides a series of high level `ewoks` nodes.

## Test

The test task can be used to test ewoks graph execution:

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.test.TestProcess"
},
```

### Inputs

| Parameter       | Type |                                    |
| --------------- | ---- | ---------------------------------- |
| raise_exception | str  | _optional_, raise a `RuntimeError` |
| sleep_time      | str  | _optional_, time to sleep for      |

### Outputs

| Parameter | Type  |                    |
| --------- | ----- | ------------------ |
| finished  | int   | 1                  |
| array     | array | 100 random numbers |

## Notify

Notify a beamline via rabbitmq:

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.notify.NotifyBeamline"
},
```

### Inputs

| Parameter | Type |
| --------- | ---- |
| beamline  | str  |
| message   | dict |

## Mimosa

Some high level Mimosa related tasks.

### StartJob

Creates a `ProcessingJob` and `AutoProcProgram` entry for the related `dataCollectionId`

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.mimosa.autoproc.StartJob"
},
```

#### Inputs

| Parameter        | Type |            |                                                                                |
| ---------------- | ---- | ---------- | ------------------------------------------------------------------------------ |
| dataCollectionId | int  |            |                                                                                |
| workflow         | str  |            | Path to the workflow json file                                                 |
| process_name     | str  |            | The name of the process, i.e. `pyFAI`                                          |
| output_filename  | str  |            | The output h5 file used to determine the root workflow directory (for logging) |
| config           | dict | _optional_ | Config passed to the workflow to be stored in ProcessingJobParameters          |
| automatic        | bool | _optional_ | Whether the job should be marked as `automatic`                                |

#### Outputs

| Parameter         | Type |                                    |
| ----------------- | ---- | ---------------------------------- |
| autoProcProgramId | int  |                                    |
| procesingJobId    | int  |                                    |
| output_directory  | str  | The working directory for this job |

### EndJob

Marks an `AutoProcProgram` as completed successfully and updates the relevant timestamp and status

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.mimosa.autoproc.EndJob"
},
```

#### Inputs

| Parameter         | Type |     |                       |
| ----------------- | ---- | --- | --------------------- |
| autoProcProgramId | int  |     |                       |
| nxdata_url        | str  |     | The resulting h5 file |

### ErrorHandler

Error handler for failed tasks, updates the related `AutoProcProgram` with the failed state, and captures the `WorkflowException` to file.

!!! note
    `default_error_node` cannot be used as `ErrorHandler` needs `autoProcProgramId` from `StartJob`. Therefore it must be linked to each task that might failed

```json
{
  "id": "error_handler",
  "task_type": "class",
  "task_identifier": "sidecar.ewoks.mimosa.autoproc.ErrorHandler"
}
```

then linked:

```json
{
    "source": "1",
    "target": "sidecar.ewoks.mimosa.autoproc.ErrorHandler0",
    "on_error": true,
    "map_all_data": true
    },
```

#### Inputs

| Parameter         | Type |                                                                        |
| ----------------- | ---- | ---------------------------------------------------------------------- |
| autoProcProgramId | int  |                                                                        |
| output_directory  | str  | `output_directory` from `StartJob` to know where to write the log file |
| WorkflowException | dict | Automatically mapped from the failed task                              |

### GetMetadata

Retrieves the Mimosa metadata for the related `dataCollectionId`

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.mimosa.metadata.GetMetadata"
},
```

#### Inputs

| Parameter        | Type |
| ---------------- | ---- |
| dataCollectionId | int  |

#### Outputs

| Parameter | Type |                                                  |
| --------- | ---- | ------------------------------------------------ |
| metadata  | dict | Mimosa related metadata for this data collection |

### Add AutoProcProgramAttachment

Add an AutoProcProgramAttachment:

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.mimosa.autoproc.AddAttachment"
},
```

#### Inputs

| Parameter         | Type |                                            |
| ----------------- | ---- | ------------------------------------------ |
| autoProcProgramId | int  |                                            |
| nxdata_url        | str  | The file to be attached as a `Result` file |
| importanceRank    | str  | _optional_                                 |

#### Outputs

| Parameter                   | Type |
| --------------------------- | ---- |
| autoProcProgramAttachmentId | int  |

### Add AutoProcProgramMessage

Add an AutoProcProgramMessage:

```json
{
    "id": "process",
    "task_type": "class",
    "task_identifier": "sidecar.ewoks.mimosa.autoproc.AddMessage"
},
```

#### Inputs

| Parameter         | Type                             |
| ----------------- | -------------------------------- |
| autoProcProgramId | int                              |
| severity          | enum(`ERROR`, `WARNING`, `INFO`) |
| message           | str                              |
| description       | str                              |

#### Outputs

| Parameter                | Type |
| ------------------------ | ---- |
| autoProcProgramMessageId | int  |
