`sidecar.server`: Start the sidecar server

`sidecar.mimas`: Simulate or execute mimas for the specified data collection

`sidecar.execute_graph`: Execute an `ewoks` graph for a data collection

`sidecar.notify`: Send a beamline a notification message

`sidecar.system_test`: Run the system tests
