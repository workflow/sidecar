Mimas is a decision maker, 100% borrowed from [zocalo](https://github.com/diamondlightsource/python-zocalo), allowing the acquisition software to be completely decoupled from processing that occurs.

The task can be executed from python as follows:

```python
from sidecar.kombu.message import send_message
from sidecar.server.messages import MimasMessage

event = "start" # or "end"
send_message(
    MimasMessage(
        event=event,
        dataCollectionId=dataCollectionId
    )
)
```

Or via the cli tool `sidecar.mimas`

## Specifications

Which tasks are started based on an event are defined by the `MIMAS_SPECIFICATIONS` environment variable, some examples are provided in `sidecar.mimas.specifications`.

Specifications can be predicated on a number of parameters and metadata injected from Mimosa:

```python
from sidecar.mimas.specification import (
    match_specification,
    EventSpecification,
    ExperimentTypeSpecification,
)
from sidecar.mimas.classes import (
    MimasScenario,
    MimasGraphInvocation,
    MimasEvent,
    MimasExperimentType,
)

is_start = EventSpecification(MimasEvent.START)
is_end = EventSpecification(MimasEvent.END)

is_xrf_map = ExperimentTypeSpecification(MimasExperimentType.XRF_MAP)


@match_specification(is_end & is_xrf_map)
def handle_xrf_map_end(scenario: MimasScenario) -> list[MimasGraphInvocation]:
    return [
        MimasGraphInvocation(dataCollectionId=scenario.dataCollectionId, graph="pymca")
    ]
```
