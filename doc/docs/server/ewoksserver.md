Sidecar executes ewoks jobs via the ewoksserver [REST API](https://ewoksserver.readthedocs.io/en/latest/restapi.html).

The server will create a `ProcessingJob` and `AutoProcProgram` entry on launch, and update these with success/failure and run time depending on the execution status. The ewoks events are written to a log file and attached as an `AutoProcProgramAttachment`.

In addition it adds some Mimosa parameters as `inputs`:

```python
inputs = [
    {"name": "metadata", "value": metadata},
    {"name": "taskDirectory", "value": taskDirectory},
    {"name": "dataCollectionId", "value": dataCollectionId},
    {"name": "processingJobId", "value": processingJobId},
    {"name": "autoProcProgramId", "value": autoProcProgramId},
]
```

The `metadata` input contains the relevant Mimosa metadata from the associated datacollection, which looks something like:

```python
{
  "fileTemplate": "sdf__roi6_4.h5",
  "imageDirectory": "/data/bl/tmp/blc00001/sdf/sdf__roi6_4",
  "imageContainerSubPath": None,
  "numberOfImages": None,
  "exposureTime": None,
  "runStatus": "Failed",
  "experimentType": "XRF map",
  "beamLineName": "bl",
  "session": "blc00001-1",
  "proposal": "blc00001",
  "sequence": None,
  "steps_x": None,
  "steps_y": None,
  "workingDirectory": "/data/bl/tmp/blc00001/sdf/sdf__roi6_4"
}
```
