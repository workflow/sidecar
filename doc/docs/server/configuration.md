Sidecar is configured via environment variables or via a `.env` file. Environment variables always take precedence over `.env`:

```bash
# Mimosa Credentials
MIMOSA_DATABASE_URL=localhost:3306/test
MIMOSA_DATABASE_USER=test
MIMOSA_DATABASE_PASSWORD=test

# Broker configuration (default rabbitmq)
BROKER_URL=amqp://user:pass@localhost:5672/vhost
# The default queue name
BROKER_QUEUE=default

# Graylog
GRAYLOG_URL=localhost:12201

# Mimas specifications
MIMAS_SPECIFICATIONS=sidecar.mimas.specifications

# Path to system test configuration
SYSTEM_TEST_CONFIG=system_test.yml
```

## Processed data location

Processed data locations are determined by `blissoda` in accordance with the ESRF data policy
